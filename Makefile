#
# 'make depend' uses makedepend to automatically generate dependencies 
#               (dependencies are added to end of Makefile)
# 'make'        build executable file 'mycc'
# 'make clean'  removes all .o and executable files
#

# define the C++ compiler to use
CC = g++

# define any compile-time flags
CFLAGS = -Wall -O3 -std=c++11 #-pg

# define any directories containing header files other than /usr/include
#
INCLUDES = -I ../include/
SRCDIR = ./src/
# define library paths in addition to /usr/lib
# if I wanted to include libraries not in /usr/lib I'd specify
# their path using -Lpath, something like:

LIBS = -lm

# define the C++ source files
SRCS = $(SRCDIR)node.cpp $(SRCDIR)main.cpp $(SRCDIR)datafile.cpp $(SRCDIR)route.cpp $(SRCDIR)solution.cpp $(SRCDIR)solutionconstructor.cpp $(SRCDIR)localsearch.cpp $(SRCDIR)perturbation.cpp $(SRCDIR)ils.cpp $(SRCDIR)validator.cpp $(SRCDIR)movementcache.cpp $(SRCDIR)vnd.cpp
SRC_RUN = $(SRCDIR)node.cpp $(SRCDIR)runner.cpp $(SRCDIR)datafile.cpp $(SRCDIR)route.cpp $(SRCDIR)solution.cpp $(SRCDIR)solutionconstructor.cpp $(SRCDIR)localsearch.cpp $(SRCDIR)perturbation.cpp $(SRCDIR)ils.cpp $(SRCDIR)validator.cpp $(SRCDIR)movementcache.cpp $(SRCDIR)vnd.cpp
STABLER = $(SRCDIR)tabler.cpp

# define the C object files 
#
# This uses Suffix Replacement within a macro:
#   $(name:string1=string2)
#         For each word in 'name' replace 'string1' with 'string2'
# Below we are replacing the suffix .cpp of all words in the macro SRCS
# with the .o suffix
#
OBJS = $(SRCS:.cpp=.o)
OBJS_RUN = $(SRC_RUN:.cpp=.o)
OBJS_TABLER = $(STABLER:.cpp=.o)
# define the executable file 
MAIN = main
RUNNER = runner
TABLER = tabler
#
# The following part of the makefile is generic; it can be used to 
# build any executable just by changing the definitions above and by
# deleting dependencies appended to the file from 'make depend'
#

.PHONY: depend clean

all:    $(MAIN) $(RUNNER) $(TABLER)
	@echo  Main and Runner compiled

$(MAIN): $(OBJS) 
	$(CC) $(CFLAGS) $(INCLUDES) -o $(MAIN) $(OBJS) $(LIBS)
	
$(RUNNER): $(OBJS_RUN)
	$(CC) $(CFLAGS) $(INCLUDES) -o $(RUNNER) $(OBJS_RUN) $(LIBS)

$(TABLER): $(OBJS_TABLER)
	$(CC) $(CFLAGS) $(INCLUDES) -o $(TABLER) $(OBJS_TABLER) $(LIBS)
	
# this is a suffix replacement rule for building .o's from .c's
# it uses automatic variables $<: the name of the prerequisite of
# the rule(a .c file) and $@: the name of the target of the rule (a .o file) 
# (see the gnu make manual section about automatic variables)
.cpp.o: 
	$(CC) $(CFLAGS) $(INCLUDES) -c $<  -o $@

clean:
	$(RM) $(SRCDIR)*.o 
	$(RM) *.bak 

depend: $(SRCS)
	makedepend $(INCLUDES) $^

# DO NOT DELETE THIS LINE -- make depend needs it
