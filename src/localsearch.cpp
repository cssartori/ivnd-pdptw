#include "../include/localsearch.h"
#include "../include/constants.h"
#include <cmath>


/*Compares two pair shift tuples as: tp1 > tp2*/
bool pairTupleGT(PairMovementTuple tp1, PairMovementTuple tp2);

LocalSearch::LocalSearch(DataFile data) : data(""){
	this->data = data;
}


void LocalSearch::initMovementCaches(Solution *s){
	this->shift_cache.initCache(s->getNumRoutes());
	this->exchange_cache.initCache(s->getNumRoutes());
	this->rearrange_cache.initCache(s->getNumRoutes());
	this->shiftR2_cache.initCache(s->getNumRoutes());
}

void LocalSearch::clearMovementCaches(){
	this->shift_cache.clearCache();
	this->exchange_cache.clearCache();
	this->rearrange_cache.clearCache();
	this->shiftR2_cache.clearCache();
}


void LocalSearch::setModifiedRoutesCaches(int r1, int r2){
	this->shift_cache.setModifiedRoute1(r1);
	this->shift_cache.setModifiedRoute2(r2);
	
	this->shiftR2_cache.setModifiedRoute1(r1);
	this->shiftR2_cache.setModifiedRoute2(r2);
	
	this->exchange_cache.setModifiedRoute1(r1);
	this->exchange_cache.setModifiedRoute2(r2);
	
	this->rearrange_cache.setModifiedRoute1(r1);
	this->rearrange_cache.setModifiedRoute2(r2);
}

void LocalSearch::clearRoutesCache(int r1, int r2){
	this->shift_cache.clearRoutes(r1, r2);
	this->shiftR2_cache.clearRoutes(r1, r2);
	this->exchange_cache.clearRoutes(r1, r2);
	this->rearrange_cache.clearRoutes(r1, r2);
}


/*Shift pairs between routes, choosing the shift that minimizes the total cost*/
Solution LocalSearch::pdShiftR2(Solution s){
	//solution must have at least 2 routes
	if(s.getNumRoutes() < 2) return s;
	
	PairMovementTuple best_pair;
	int mr2 = 0;
	for(int kr=0;kr<s.getNumRoutes();kr++){
		mr2 += pow(s.getRoute(kr).getSize(), 2);
	}
	
	for(int r1=0;r1<s.getNumRoutes();r1++){
		for(int r2=0;r2<s.getNumRoutes();r2++){
			if(r1 == r2) continue;
			
			if(!this->shiftR2_cache.isRouteModified(r1) && !this->shiftR2_cache.isRouteModified(r2)
				&& this->shiftR2_cache.hasMovement(r1, r2)){
				PairMovementTuple new_pair = this->shiftR2_cache.getMovement(r1, r2);
				if(new_pair.u != NO_NODE && (s.getDistance() > s.getDistance()+new_pair.cost || new_pair.lessr)){
					if(best_pair.u == NO_NODE || pairTupleGT(best_pair, new_pair)){
						best_pair = new_pair;
					}
				}
				continue;
			}
			
			Route route1 = s.getRoute(r1);
			Route route2 = s.getRoute(r2);
			PairMovementTuple bp_r1_r2;
			int u=route1.getForward(0); //first node to try to shift from route 1 to route 2
			while(u != 0){
				int v = this->data.getNode(u).getPair();
				double c_rm_u = c_rm(route1.getBackward(u), u, route1.getForward(u))+c_rm(route1.getBackward(v), v, route1.getForward(v));
				//considering only the pickups of each pair
				if(this->data.getNode(u).getType() == DELIVERY_NODE){
					u = route1.getForward(u); continue;
				}
				
				PairMovementTuple new_pair = searchInsertionLocation2(&route2, u);
				if(new_pair.u != NO_NODE){
					new_pair.cost += c_rm_u;
					new_pair.r1 = r1;
					new_pair.r2 = r2;
					new_pair.lessr = (route1.getSize() == 2);
					new_pair.mr = mr2;
					new_pair.mr -= (pow(s.getRoute(r1).getSize(), 2)+pow(s.getRoute(r2).getSize(),2));
					new_pair.mr += (pow(s.getRoute(r1).getSize()-2,2)+pow(s.getRoute(r2).getSize()+2,2));

					if(new_pair.mr >= mr2 || s.getDistance() > s.getDistance()+new_pair.cost || (route1.getSize() == 2)){
						if(best_pair.u == NO_NODE || new_pair.mr > best_pair.mr){
							best_pair = new_pair;
						}else if(new_pair.mr == best_pair.mr && pairTupleGT(best_pair, new_pair))
							best_pair = new_pair;
							
						if(bp_r1_r2.u == NO_NODE || best_pair.mr > bp_r1_r2.mr)
							bp_r1_r2 = new_pair;
						else if(bp_r1_r2.mr == best_pair.mr && pairTupleGT(best_pair, bp_r1_r2))
							bp_r1_r2 = new_pair;
					}
				}				
				u = route1.getForward(u);
			}
			this->shiftR2_cache.addMovement(r1, r2, bp_r1_r2);
		}
	}
	
	/*If there is a shift that improves the solution the maximum*/
	if(best_pair.u != NO_NODE){
		Route route1 = s.getRoute(best_pair.r1);
		Route route2 = s.getRoute(best_pair.r2);
		
		route1 = removePairFromRoute(route1, best_pair.u);
		insertInRoute(&route2, best_pair.back_u, best_pair.u);
		insertInRoute(&route2, best_pair.back_v, best_pair.v);
		
		calculateFTimeSlack(&route1);
		calculateFTimeSlack(&route2);
		
		this->setModifiedRoutesCaches(best_pair.r1, best_pair.r2);
		
		if(route1.getSize() == 0){
			s.removeRoute(best_pair.r1);
			if(best_pair.r2 > best_pair.r1) best_pair.r2--;
			this->clearMovementCaches();
		}else{
			s.updateRoute(best_pair.r1, route1);
			this->clearRoutesCache(best_pair.r1, best_pair.r2);	
		}

		s.updateRoute(best_pair.r2, route2);
	}
	
	return s;
}

/*Shift pairs between routes, choosing the shift that minimizes the total cost*/
Solution LocalSearch::pdShift(Solution s){
	//solution must have at least 2 routes
	if(s.getNumRoutes() < 2) return s;
	
	PairMovementTuple best_pair;
	
	for(int r1=0;r1<s.getNumRoutes();r1++){
		for(int r2=0;r2<s.getNumRoutes();r2++){
			if(r1 == r2) continue;
			
			if(!this->shift_cache.isRouteModified(r1) && !this->shift_cache.isRouteModified(r2)
				&& this->shift_cache.hasMovement(r1, r2)){
				PairMovementTuple new_pair = this->shift_cache.getMovement(r1, r2);
				if(new_pair.u != NO_NODE && (s.getDistance() > s.getDistance()+new_pair.cost || new_pair.lessr)){
					if(best_pair.u == NO_NODE || pairTupleGT(best_pair, new_pair)){
						best_pair = new_pair;
					}
				}
				continue;
			}
			
			Route route1 = s.getRoute(r1);
			Route route2 = s.getRoute(r2);
			PairMovementTuple bp_r1_r2; //best movement between r1 and r2
			int u=route1.getForward(0); //first node to try to shift from route 1 to route 2
			while(u != 0){
				int v = this->data.getNode(u).getPair();
				double c_rm_u = c_rm(route1.getBackward(u), u, route1.getForward(u))+c_rm(route1.getBackward(v), v, route1.getForward(v));
				//considering only the pickups of each pair
				if(this->data.getNode(u).getType() == DELIVERY_NODE){
					u = route1.getForward(u); continue;
				}
				
				PairMovementTuple new_pair = searchInsertionLocation2(&route2, u);
				if(new_pair.u != NO_NODE){
					new_pair.cost += c_rm_u;
					new_pair.r1 = r1;
					new_pair.r2 = r2;
					new_pair.lessr = (route1.getSize() == 2);
					if(s.getDistance() > s.getDistance()+new_pair.cost || (route1.getSize() == 2)){
						if(best_pair.u == NO_NODE || pairTupleGT(best_pair, new_pair))
							best_pair = new_pair;
						if(bp_r1_r2.u == NO_NODE || pairTupleGT(bp_r1_r2, new_pair))
							bp_r1_r2 = new_pair;
					}
				}				
				u = route1.getForward(u);
			}

			this->shift_cache.addMovement(r1, r2, bp_r1_r2);
			this->shift_cache.addMovement(r1, r2, bp_r1_r2);
		}
	}
	
	/*If there is a shift that improves the solution the maximum*/
	if(best_pair.u != NO_NODE){
		Route route1 = s.getRoute(best_pair.r1);
		Route route2 = s.getRoute(best_pair.r2);

		route1 = removePairFromRoute(route1, best_pair.u);
		insertInRoute(&route2, best_pair.back_u, best_pair.u);
		insertInRoute(&route2, best_pair.back_v, best_pair.v);
		
		calculateFTimeSlack(&route1);
		calculateFTimeSlack(&route2);
		
		this->setModifiedRoutesCaches(best_pair.r1, best_pair.r2);
		
		if(route1.getSize() == 0){
			s.removeRoute(best_pair.r1);
			if(best_pair.r2 > best_pair.r1) best_pair.r2--;
			this->clearMovementCaches();
		}else{
			s.updateRoute(best_pair.r1, route1);
			this->clearRoutesCache(best_pair.r1, best_pair.r2);
		}

		s.updateRoute(best_pair.r2, route2);
	}
	
	return s;
}



Solution LocalSearch::pdExchange(Solution s){
	//solution must have at least 2 routes
	if(s.getNumRoutes() < 2) return s;
	
	PairMovementTuple best_pair_1;
	PairMovementTuple best_pair_2;

	for(int r1=0;r1<s.getNumRoutes();r1++){
		Route route1 = s.getRoute(r1);
		for(int r2=r1+1;r2<s.getNumRoutes();r2++){

			if(!this->exchange_cache.isRouteModified(r1) && !this->exchange_cache.isRouteModified(r2)
				&& this->exchange_cache.hasMovement(r1, r2)){
				PairMovementTuple np1 = this->exchange_cache.getMovement(r1, r2, 0);
				PairMovementTuple np2 = this->exchange_cache.getMovement(r1, r2, 1);
				if(np1.u != NO_NODE && np2.u != NO_NODE && s.getDistance() > s.getDistance()+np1.cost+np2.cost){
					if(best_pair_1.u == NO_NODE || (best_pair_1.cost+best_pair_2.cost) > (np1.cost+np2.cost)){
						best_pair_1 = np1;
						best_pair_2 = np2;
					}
				}
				continue;
			}

			Route route2 = s.getRoute(r2);
			
			PairMovementTuple bp_r1_r2_1;
			PairMovementTuple bp_r1_r2_2;
			int u = route1.getForward(0);
			while(u!=0){
				if(this->data.getNode(u).getType() == DELIVERY_NODE){
					u = route1.getForward(u); continue;
				}
				Route rr1 = removePairFromRoute(route1, u);
				double c_rm_u = rr1.getDistance()-route1.getDistance();
				
				int x = route2.getForward(0);
				while(x!=0){
					if(this->data.getNode(x).getType() == DELIVERY_NODE){
						x = route2.getForward(x); continue;
					}
					
					calculateFTimeSlack(&rr1);
					PairMovementTuple np1 = searchInsertionLocation2(&rr1, x);
					if(np1.u == NO_NODE){
						x = route2.getForward(x);
						continue;
					}	
						
					Route rr2 = removePairFromRoute(route2, x);
					calculateFTimeSlack(&rr2);
					double c_rm_x = rr2.getDistance()-route2.getDistance();
												
					PairMovementTuple np2 = searchInsertionLocation2(&rr2, u);
					if(np2.u == NO_NODE){
						x = route2.getForward(x);
						continue;
					}
		
					np1.cost += c_rm_u;
					np2.cost += c_rm_x;
					np1.r1 = r1; np1.r2 = r2;
					np2.r1 = r1; np2.r2 = r2;
					double tc = np1.cost + np2.cost;
					
					if(s.getDistance() > s.getDistance()+tc){
						if(best_pair_1.u == NO_NODE || (best_pair_1.cost+best_pair_2.cost) > tc){
							best_pair_1 = np1;
							best_pair_2 = np2;
						}
						if(bp_r1_r2_1.u == NO_NODE || (bp_r1_r2_1.cost+bp_r1_r2_2.cost) > tc){
							bp_r1_r2_1 = np1;
							bp_r1_r2_2 = np2;
						}
					}					
					x = route2.getForward(x);
				}
												
				u = route1.getForward(u);
			}
			this->exchange_cache.addMovement(r1, r2, bp_r1_r2_1);
			this->exchange_cache.addMovement(r1, r2, bp_r1_r2_2);
			
		}
	}
	
	if(best_pair_1.u != NO_NODE){
		Route route1 = s.getRoute(best_pair_1.r1);
		Route route2 = s.getRoute(best_pair_1.r2);

		route1 = removePairFromRoute(route1, best_pair_2.u);
		route2 = removePairFromRoute(route2, best_pair_1.u);
		
		insertInRoute(&route1, best_pair_1.back_u, best_pair_1.u);
		insertInRoute(&route1, best_pair_1.back_v, best_pair_1.v);		
		insertInRoute(&route2, best_pair_2.back_u, best_pair_2.u);
		insertInRoute(&route2, best_pair_2.back_v, best_pair_2.v);
		
		calculateFTimeSlack(&route1);
		calculateFTimeSlack(&route2);		
					
		s.updateRoute(best_pair_1.r1, route1);
		s.updateRoute(best_pair_1.r2, route2);

		this->setModifiedRoutesCaches(best_pair_1.r1, best_pair_1.r2);
		this->clearRoutesCache(best_pair_1.r1, best_pair_1.r2);
	}
		
	return s;
}


Solution LocalSearch::pdRearrange(Solution s){
	
	PairMovementTuple best_pair;

	for(int r=0;r<s.getNumRoutes();r++){
		if(!this->rearrange_cache.isRouteModified(r) && this->rearrange_cache.hasMovement(r, r)){
			PairMovementTuple np = this->rearrange_cache.getMovement(r, r);
			if(s.getDistance() > s.getDistance() + np.cost){
				if(best_pair.u == NO_NODE || best_pair.cost > np.cost){
					best_pair = np;
				}
			}
			continue;
		}
		
		PairMovementTuple bp_r;
		Route route = s.getRoute(r);
		
		int u = route.getForward(0);
		while(u != 0){
			if(this->data.getNode(u).getType() == DELIVERY_NODE){
				u = route.getForward(u); continue;
			}	
			int v = this->data.getNode(u).getPair();
			Route rr1 = removePairFromRoute(route, u);
			double c_rm_u = rr1.getDistance()-route.getDistance();
			calculateFTimeSlack(&rr1);
			PairMovementTuple new_pair = searchInsertionLocation2(&rr1, u);
			if(new_pair.back_u != route.getBackward(u) || new_pair.back_v != route.getBackward(v)){
				double c_diff = new_pair.cost+c_rm_u;
				if(s.getDistance() > s.getDistance() + c_diff){
					if(best_pair.u == NO_NODE || best_pair.cost > c_diff){
						best_pair = new_pair;
						best_pair.cost = c_diff;
						best_pair.r1 = r;
						best_pair.r2 = r;
					}
					if(bp_r.u == NO_NODE || bp_r.cost > c_diff){
						bp_r = new_pair;
						bp_r.cost = c_diff;
						bp_r.r1 = r;
						bp_r.r2 = r;
					}
				}
			}
			
			u = route.getForward(u);
		}
		this->rearrange_cache.addMovement(r, r, bp_r);
	}
	
	if(best_pair.u != NO_NODE){
		Route route = s.getRoute(best_pair.r1);
		route = removePairFromRoute(route, best_pair.u);
		insertInRoute(&route, best_pair.back_u, best_pair.u);
		insertInRoute(&route, best_pair.back_v, best_pair.v);
		
		calculateFTimeSlack(&route);
				
		s.updateRoute(best_pair.r1, route);
		
		this->setModifiedRoutesCaches(best_pair.r1, best_pair.r2);
		this->clearRoutesCache(best_pair.r1, best_pair.r2);
	}	
	
	return s;
}


PairMovementTuple LocalSearch::searchInsertionLocation2(Route *route, int u){
	PairMovementTuple best_pair;
	
	int i=0; //u will be inserted after i in route 2
	do{
		int k = route->getForward(i);
		double departure_time_i = 0.0; //time to leave node i (default is when i == 0)	
		if(i != 0)
			departure_time_i = std::max(route->getReachTime(i),(double) this->data.getNode(i).getETW()) + this->data.getNode(i).getST(); //time to leave node i
		double rtime_u = departure_time_i + this->data.getTime(i, u);
		double start_serv_u = std::max(rtime_u, (double)this->data.getNode(u).getETW());
		bool valid = false;
		if(start_serv_u <= this->data.getNode(u).getLTW())
			valid = true;
		
		double departure_time_u = start_serv_u + this->data.getNode(u).getST();
		double new_rtime_k = departure_time_u + this->data.getTime(u, k);
		double new_start_serv_k = std::max(new_rtime_k, (double)this->data.getNode(k).getETW());
		double old_start_serv_k = std::max(route->getReachTime(k), (double)this->data.getNode(k).getETW());
		
		if(valid && (new_start_serv_k - old_start_serv_k == 0)){
			valid = true;
		}else if(valid && (new_rtime_k - route->getReachTime(k)) <= route->getFTimeSlack(k)){
			valid = true;
		}else{
			valid = false;
		}
				
		//checks if inserting u after i in route does not violate constraints
		if(valid){
			route->insertNode(i, u);
			int v = this->data.getNode(u).getPair(); //the delivery pair of pickup node u
			int j = u; //v will be inserted after j in route
			
			double rtime_j = departure_time_i + this->data.getTime(i, j); //time to reach node j (in this case, u)
			
			route->setAccCapacity(u, route->getAccCapacity(route->getBackward(u)));
			/*try to insert node v after every node j untill the depot*/
			while(j != 0){
				/*If the vehicle is overloaded at node j, node v can't be inserted AFTER it (solution will be infeasible)'*/
				if(route->getAccCapacity(j)+this->data.getNode(u).getDemand() > this->data.getVehiclesCapacity())
					break;
								
				int m = route->getForward(j);
				double departure_time_j = std::max(rtime_j, (double)this->data.getNode(j).getETW())+this->data.getNode(j).getST();
				double rtime_v = departure_time_j + this->data.getTime(j, v);
				double start_serv_v = std::max(rtime_v, (double)this->data.getNode(v).getETW());
				bool valid2 = true;
				
				if(start_serv_v > this->data.getNode(v).getLTW()){
					valid2 = false;
				}
				
				double departure_time_v = start_serv_v + this->data.getNode(v).getST();
				double new_rtime_m = departure_time_v + this->data.getTime(v, m);
				double new_start_serv_m = std::max(new_rtime_m, (double)this->data.getNode(m).getETW());
				double old_start_serv_m = std::max(route->getReachTime(m), (double)this->data.getNode(m).getETW());
				
				if(valid2 && (new_start_serv_m - old_start_serv_m == 0)){
					valid = true;
				}
				else if(valid2 && (new_rtime_m - route->getReachTime(m)) <= (route->getFTimeSlack(m)/*-fts_diff*/)){
					valid2 = true;
				}
				else{
					valid2 = false;
				}
										
				/*Checks if inserting v after j in route 2 does not violate constraints*/
				if(valid2){
					double ci = c_in(i, u, route->getForward(u)) + c_in(j, v, route->getForward(j));
													
					//only set pair if solution gets better
					if(ci < best_pair.cost){ 
						PairMovementTuple new_pair;
						new_pair.back_u = i; new_pair.u = u;
						new_pair.back_v = j; new_pair.v = v;
						new_pair.cost = ci;
						best_pair = new_pair;
					}
				}
				//double departure_time_j = std::max(rtime_j,(double) this->data.getNode(j).getETW()) + this->data.getNode(j).getST();
				rtime_j = departure_time_j + this->data.getTime(j, route->getForward(j));
				j = route->getForward(j);
			}
			route->removeNode(u);
		}		
		i = route->getForward(i);
	}while(i != 0);
	
	return best_pair;	
}



/*Compares two pair shift tuples as: tp1 > tp2*/
bool pairTupleGT(PairMovementTuple tp1, PairMovementTuple tp2){
	bool ret = false;
		
	if(tp1.lessr){
		if(tp2.lessr && tp1.cost > tp2.cost)
			ret = true;
	}else{
		if(tp2.lessr)
			ret = true;
		else if(tp1.cost > tp2.cost)
			ret = true;
	}
	return ret;
}

/*Cost to insert node u between i and j*/
double LocalSearch::c_in(int i, int u, int j){
	return (this->data.getDistance(i, u) + this->data.getDistance(u, j) - this->data.getDistance(i, j));
}

/*Cost to remove node u between i and j*/
double LocalSearch::c_rm(int i, int u, int j){
	return -(this->data.getDistance(i, u) + this->data.getDistance(u, j) - this->data.getDistance(i, j));
}

/*Insert node u after node i in route, updating information accordingly*/
void LocalSearch::insertInRoute(Route *route, int i, int u){
	/*set accumulated capacity for node u*/
	route->setAccCapacity(u, route->getAccCapacity(i)+this->data.getNode(u).getDemand());
	
	double departure_time_i = 0.0;//time to leave node i
	if(i != 0)
	 	departure_time_i = std::max(route->getReachTime(i), (double)this->data.getNode(i).getETW()) + this->data.getNode(i).getST();
	
	route->setReachTime(u, departure_time_i + this->data.getTime(i, u)); //time to reach node u
	double departure_time_u = std::max(route->getReachTime(u), (double)this->data.getNode(u).getETW()) + this->data.getNode(u).getST(); //time to leave node u

	int node = route->getForward(i); //future forward of node u
	double rtime_n = departure_time_u + this->data.getTime(u, node); //time to reach node
	//update all reach time and capacity information
	do{
		route->setReachTime(node, rtime_n);
		route->updateAccCapacity(node, this->data.getNode(u).getDemand());

		double departure_time_n = std::max(rtime_n ,(double) this->data.getNode(node).getETW())+this->data.getNode(node).getST();
		rtime_n = departure_time_n + this->data.getTime(node, route->getForward(node));
		node = route->getForward(node);	
	}while(node != route->getForward(0));
	
	//update route distance
	double addDist = c_in(i, u, route->getForward(i));
	route->insertNode(i, u);	
	route->updateDistance(addDist);
		
}

/*Remove node u and its delivery pair from route, updating information accordingly*/
Route LocalSearch::removePairFromRoute(Route route, int u){
		int v = this->data.getNode(u).getPair();
	int i = route.getBackward(u);
	double departure_time_i = 0.0;
	
	if(i != 0)
	 	departure_time_i = std::max(route.getReachTime(i), (double)this->data.getNode(i).getETW()) + this->data.getNode(i).getST();

	int node = route.getForward(u);
	int add_cap = -this->data.getNode(u).getDemand();
	if(node == v){
		node = route.getForward(node);
		add_cap = 0;
	}
	double rtime_n = departure_time_i + this->data.getTime(i, node); //time to reach node 

	//update all reach time and capacity information
	while(node != route.getForward(0)){
		if(node == v){
			if(route.getBackward(node) == u){
				rtime_n -= this->data.getTime(i, node);
				rtime_n += this->data.getTime(i, route.getForward(node));
			}else{
				rtime_n -= this->data.getTime(route.getBackward(node), node);
				rtime_n += this->data.getTime(route.getBackward(node), route.getForward(node));
			}
			add_cap = 0;
		}else{	
			route.setReachTime(node, rtime_n);
			route.updateAccCapacity(node, add_cap);

			double departure_time_n = std::max(rtime_n ,(double) this->data.getNode(node).getETW())+this->data.getNode(node).getST();
			rtime_n = departure_time_n + this->data.getTime(node, route.getForward(node));
		}
		node = route.getForward(node);	
	}
	
	//update route distance
	double addDist = c_rm(route.getBackward(u), u, route.getForward(u));
	route.removeNode(u);
	addDist += c_rm(route.getBackward(v), v, route.getForward(v));	
	route.removeNode(v);	
	route.updateDistance(addDist);
		
	return route;
}


/*Check if inserting u after node i in route is a valid movement*/
bool LocalSearch::isValidInsertion(Route *route, int i, int u, double rtime_i){
	bool valid = false;
	double departure_time_i = 0.0; //default case is when i == 0
	
	if(i != 0)
		departure_time_i = std::max(rtime_i, (double)this->data.getNode(i).getETW()) + this->data.getNode(i).getST(); //time to leave node i
		
	double rtime_u = departure_time_i + this->data.getTime(i, u); //time to reach node u from i
	double start_serv_u = std::max(rtime_u, (double)this->data.getNode(u).getETW()); //time for service to start at node u
	
	//If service start at node u before its latest time window, and if the vehicle is not overloaded
	if(start_serv_u <= this->data.getNode(u).getLTW() && route->getAccCapacity(i)+this->data.getNode(u).getDemand() <= this->data.getVehiclesCapacity()){
		double departure_time_u = start_serv_u + this->data.getNode(u).getST(); //time to leave node u
		int node = route->getForward(i); //future forward of node u
		double rtime_n = departure_time_u + this->data.getTime(u, node); //time to reach node
		valid = true;
		Node nd = this->data.getNode(node);
		//Check each following time window so that there is no violation of it
		do{
			double departure_time_n = std::max(rtime_n, (double)nd.getETW());
			if(departure_time_n > nd.getLTW()){
				valid = false;
				break;
			}else if(departure_time_n == std::max(route->getReachTime(node), (double)nd.getETW())){
				//if the time to start service at node is the same as before, the rest of the route will have no modifications
				break;
			}
			departure_time_n += nd.getST();
			rtime_n = departure_time_n + this->data.getTime(node, route->getForward(node));
			node = route->getForward(node);
			nd = this->data.getNode(node);
		}while(node != route->getForward(0));
	}
	
	return valid;
}


void LocalSearch::calculateFTimeSlack(Route *route){
	int node = route->getForward(0);
	
	node = 0;
	double fts_n1 = this->data.getNode(node).getLTW() - std::max(route->getReachTime(node), (double)this->data.getNode(node).getETW());
	route->setFTimeSlack(node, fts_n1);
	node = route->getBackward(node);
	do{
		double start_serv_n = std::max(route->getReachTime(node), (double)this->data.getNode(node).getETW());
		double fts_n = this->data.getNode(node).getLTW() - start_serv_n;
		fts_n = std::min(fts_n1, fts_n) + start_serv_n - route->getReachTime(node);
		fts_n1 = fts_n;
		route->setFTimeSlack(node, fts_n);
		node = route->getBackward(node);
	}while(node != 0);
	
}

Solution LocalSearch::calculateAllFTimeSlack(Solution s){
	for(int r=0;r<s.getNumRoutes();r++){
		Route rr = s.getRoute(r);
		calculateFTimeSlack(&rr);
		s.setRoute(r, rr);
	}
	return s;
}


