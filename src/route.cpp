#include "../include/route.h"
#include "../include/constants.h"
#include "../include/errors.h"
#include <stdexcept>
#include <cstdio>

Route::Route(int n){
	this->forward.assign(n, NO_NODE);
	this->forward[0] = 0;
	this->backward.assign(n, NO_NODE);
	this->backward[0] = 0;
	this->reach_time.assign(n, 0.0);
	this->acc_cap.assign(n, 0);
	this->ftime_slack.assign(n, 0.0);
	this->size_r = 0;
	this->distance = 0;
}

/*Return the forward node to the ith in this route*/
int Route::getForward(int i){
//	if( i >= (int)this->forward.size() || i < 0)
//		throw std::out_of_range(NODE_INDX_OFR_MSG);

//	if(this->forward[i] == NO_NODE)	
//		throw std::out_of_range(NODE_INDX_NOT_IN_ROUTE_MSG);

	return this->forward[i];
}

/*Return the backward node to the ith in this route*/
int Route::getBackward(int i){
//	if( i >= (int)this->forward.size() || i < 0)
//		throw std::out_of_range(NODE_INDX_OFR_MSG);

//	if(this->backward[i] == NO_NODE)	
//		throw std::out_of_range(NODE_INDX_NOT_IN_ROUTE_MSG);

	return this->backward[i];
}

/*Return the accumulated time to get to node i*/
double Route::getReachTime(int i){
	if( i >= (int)this->forward.size() || i < 0){
		printf("Node %i\n", i);
		throw std::out_of_range(NODE_INDX_OFR_MSG);
	}

	return this->reach_time[i];
}

/*Return the accumulated capacity when reaching node i*/
int Route::getAccCapacity(int i){
	if( i >= (int)this->forward.size() || i < 0)
		throw std::out_of_range(NODE_INDX_OFR_MSG);

	return this->acc_cap[i];
}

/*Insert node i as the forward of b*/
void Route::insertNode(int b, int i){
	if( i >= (int)this->forward.size() || i < 0 || b >= (int)this->forward.size() || b < 0)
		throw std::out_of_range(NODE_INDX_OFR_MSG);
	if(this->backward[i] != NO_NODE || this->forward[i] != NO_NODE){
		printf("%i -> %i -> %i\n", this->backward[i], i,  this->forward[i]);
		printf("%i -> %i\n", b, i);
		throw std::out_of_range(NODE_ALREADY_IN_ROUTE_MSG);
	}
	if(this->backward[b] == NO_NODE)
		throw std::out_of_range(NODE_INDX_NOT_IN_ROUTE_MSG);

	this->forward[i] = this->forward[b];
	this->backward[this->forward[i]] = i;
	this->forward[b] = i;
	this->backward[i] = b;
	this->size_r += 1;
}

/*Remove the node i from route*/
void Route::removeNode(int i){
	if(this->backward[i] == NO_NODE)
		throw std::out_of_range(NODE_INDX_NOT_IN_ROUTE_MSG);
		
	this->forward[this->backward[i]] = this->forward[i];
	this->backward[this->forward[i]] = this->backward[i];
	this->forward[i] = NO_NODE;
	this->backward[i] = NO_NODE;
	this->acc_cap[i] = 0;
	this->reach_time[i] = 0.0;
	this->size_r -= 1;
}

/*Update the route distance with the addition cost*/
void Route::updateDistance(double addCost){
	this->distance += addCost;
}

/*Update the accumulated time to get to node i*/
void Route::updateReachTime(int i, double addTime){
	if( i >= (int)this->forward.size() || i < 0)
		throw std::out_of_range(NODE_INDX_OFR_MSG);

	if(this->backward[i] == NO_NODE)	
		throw std::out_of_range(NODE_INDX_NOT_IN_ROUTE_MSG);

	this->reach_time[i] += addTime;
}

void Route::setReachTime(int i, double time){
	if( i >= (int)this->forward.size() || i < 0)
		throw std::out_of_range(NODE_INDX_OFR_MSG);

	this->reach_time[i] = time;
}

/*Update the accumulated capacity to get to node i*/
void Route::updateAccCapacity(int i, int addCap){
	if( i >= (int)this->forward.size() || i < 0)
		throw std::out_of_range(NODE_INDX_OFR_MSG);

	if(this->backward[i] == NO_NODE)	
		throw std::out_of_range(NODE_INDX_NOT_IN_ROUTE_MSG);

	this->acc_cap[i] += addCap;
}

void Route::setAccCapacity(int i, int cap){
	if( i >= (int)this->forward.size() || i < 0)
		throw std::out_of_range(NODE_INDX_OFR_MSG);

	this->acc_cap[i] = cap;
}

int Route::getSize(){
	return this->size_r;
}

double Route::getDistance(){
	return this->distance;
}

void Route::setDistance(double d){
	this->distance = d;
}

void Route::print(){
	int node = this->forward[0];
	printf("***\n0\n");
	do{
		printf("%i\n", node);
		node = this->forward[node];
	}while(node != 0);
	printf("0\n***\n");
}

	
bool Route::isNodeInRoute(int i){
	if(this->forward[i] != NO_NODE || this->backward[i] != NO_NODE)
		return true;
	
	return false;
}


void Route::setFTimeSlack(int i, double fts){
	this->ftime_slack[i] = fts;
}

double Route::getFTimeSlack(int i){
	return this->ftime_slack[i];
}

