#include "../include/vnd.h"

VND::VND(DataFile *data, LocalSearch *ls){
	this->data = data;
	this->ls = ls;
}

Solution VND::run(Solution s, int max_iter, int *ls_index){
		ls->initMovementCaches(&s);
		Solution sb = s;
		int iter = 0;
		int k=0;
		
		while(k < 4 && iter < max_iter){
			iter++;
			Solution ts = sb;
			
			switch(ls_index[k]){
				case 0:
					ts = ls->pdShiftR2(ts);
					break;
				case 1:
					ts = ls->pdRearrange(ts);
					break;
				case 2:
					ts = ls->pdExchange(ts);
					break;
				default:
					ts = ls->pdShift(ts);
			}			
			
			if(sb > ts){
				k = 0;
				sb = ts;
			}else{
				k++;
			}
		}
		
		return sb;		
}

