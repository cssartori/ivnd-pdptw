#include "../include/movementcache.h"
#include "../include/constants.h"

MovementCache::MovementCache(){
	this->mod_r1 = NO_ROUTE;
	this->mod_r2 = NO_ROUTE;
	this->nr = 0;
}

MovementCache::MovementCache(int nr){
	this->cache_matrix.assign(nr, std::vector< std::vector<PairMovementTuple> >(nr));
	this->mod_r1 = NO_ROUTE;
	this->mod_r2 = NO_ROUTE;
	this->nr = nr;
}

void MovementCache::initCache(int nr){
	this->cache_matrix.assign(nr, std::vector< std::vector<PairMovementTuple> >(nr));
	this->mod_r1 = NO_ROUTE;
	this->mod_r2 = NO_ROUTE;
	this->nr = nr;
}

void MovementCache::addMovement(int r1, int r2, PairMovementTuple pmt){
	this->cache_matrix[r1][r2].push_back(pmt);
}

PairMovementTuple MovementCache::getMovement(int r1, int r2, int p){
	return this->cache_matrix[r1][r2][p];
}	

std::vector<PairMovementTuple> MovementCache::getAllMovements(int r1, int r2){
	return this->cache_matrix[r1][r2];
}

bool MovementCache::hasMovement(int r1, int r2){
	if(this->cache_matrix[r1][r2].size() > 0)
		return true;

	return false;
}

int MovementCache::getModifiedRoute1(){
	return this->mod_r1;
}

int MovementCache::getModifiedRoute2(){
	return this->mod_r2;
}

bool MovementCache::isRouteModified(int r){
	if(r == this->mod_r1 || r == this->mod_r2)
		return true;

	return false;
}

void MovementCache::setModifiedRoute1(int mod_r1){
	this->mod_r1 = mod_r1;
}

void MovementCache::setModifiedRoute2(int mod_r2){
	this->mod_r2 = mod_r2;
}

void MovementCache::clearCache(){
	this->cache_matrix.assign(this->nr, std::vector< std::vector<PairMovementTuple> >(nr));		
	//this->cache_matrix.clear();
	this->mod_r1 = NO_ROUTE;
	this->mod_r2 = NO_ROUTE;		
}
void MovementCache::clearRoutes(int r1, int r2){
	this->cache_matrix[r1].assign(nr, std::vector<PairMovementTuple>());
	this->cache_matrix[r2].assign(nr, std::vector<PairMovementTuple>());

	for(int i=0;i<(int)cache_matrix.size();i++){
		if(i==r1 || i==r2) continue;
		cache_matrix[i][r1].clear();
		cache_matrix[i][r2].clear();
	}

}
