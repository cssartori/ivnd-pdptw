#include "../include/node.h"
	
Node::Node(int id, int x, int y, int demand, int earliest_tw, int latest_tw, int service_time, int pdpair){
	this->id = id;
	this->position = std::make_pair(x, y);
	this->demand = demand;
	this->time_window = std::make_pair(earliest_tw, latest_tw);
	this->service_time = service_time;
	this->pdpair = pdpair;

	this->type = DEPOT_NODE;
	if(demand > 0)
		this->type = PICKUP_NODE;
	else if(demand < 0)
		this->type = DELIVERY_NODE;		
}

/*Returns the node id*/
int Node::getId(){
	return this->id;
}

/*Returns the position x or y of this node*/
int Node::getPosX(){
	return this->position.first;
}

int Node::getPosY(){
	return this->position.second;
}

/*Returns the demand of this node (negative values are for delivery points)*/
int Node::getDemand(){
	return this->demand;
}

/*Returns the earliest or latest time window value for this node*/
int Node::getETW(){
	return this->time_window.first;
}

int Node::getLTW(){
	return this->time_window.second;
}

/*Returns the service time of this node*/
int Node::getST(){
	return this->service_time;
}

/*Returns the pair of this node*/
int Node::getPair(){
	return this->pdpair;
}

/*Returns true if the node is a pickup location*/
int Node::getType(){
	return this->type;
}
