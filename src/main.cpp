#include "../include/datafile.h"
#include "../include/solution.h"
#include "../include/solutionconstructor.h"
#include "../include/localsearch.h"
#include "../include/ils.h"
#include "../include/validator.h"
#include <string>
#include <iostream>
#include <cstdio>
#include <chrono>
#include <cmath>
#include <string.h>

using namespace std;

bool solve = true; //if the problem should be solved
string filename; //instance file name
bool save_sol = false; //if solution should be saved to file
string sol_filename; //solution filename (when informed)
bool save_glpk = false; //if GLPK data should be saved to file
string glpk_filename; //GLPK data filename (when informed)
int seed = 1; //seed for random number generation (Default = 1)
string read_sol_name;
bool realw_inst = false;

int max_iter_imp = 82;
int max_iter_vnd = 98;
double alpha = 0.288;
double n_swaps = 0.266;
int ls_index[4] = {3,1,2,0};

void parameterNotDefined(string par);
void usage(char **argv);
void readParameter(int argc, char **argv);
int validateSolution(DataFile data, Solution s);

int main(int argc, char **argv){
	readParameter(argc, argv);
	
	DataFile data(filename);
	
	if(realw_inst){
		if(data.readRealWorldData() == -1){
			printf("Error reading file \'%s\'\n'", filename.c_str());
			return -1;
		}
	}else{
		if(data.readData() == -1){
			printf("Error reading file \'%s\'\n'", filename.c_str());
			return -1;
		}
	}
	
	if(save_glpk){
		data.writeAsGLPKData(glpk_filename);
	}else if(solve){
		printf("Starting solver\n");
		std::chrono::time_point<std::chrono::system_clock> start, end;
    	start = std::chrono::system_clock::now();
    
		SolutionConstructor sc(data);
		Solution s = sc.insertionHeuristic();
				
		ILS ils(data, max_iter_imp, max_iter_vnd, n_swaps, alpha, seed, ls_index);
		Solution bs = ils.run();

		end = std::chrono::system_clock::now();
		std::chrono::duration<double> elapsed_seconds = end-start;
		
		printf("Ellapsed time: %.2f seconds\n", elapsed_seconds.count());
		int res = Validator::validateSolution(data, bs);
		if(res != 0)
			printf("BAD SOLUTION %i\n", res);
		if(save_sol){
			data.writeSolutionFile(sol_filename, s);
			printf("Solution saved to file %s\n", sol_filename.c_str());
		}
		printf("IS: D = %.2f | V = %i\n", s.getDistance(), s.getNumRoutes());
		printf("Best solution: D = %.2f | V = %i\n", bs.getDistance(), bs.getNumRoutes());
	}
	
	return 0;	
}


/*Read the input parameters*/
void readParameter(int argc, char **argv){

	if(argc < 3){
		usage(argv);
		exit(-1);
	}
	
	bool filename_read = false;
	
	for(int i=1;i<argc;i++){
		if(argv[i][0] == '-'){
			switch(argv[i][1]){
				case 'f'://instace file path
					i++;
					filename = argv[i];
					filename_read = true;
					break;
				case 'o'://solution file path
					i++;
					sol_filename = argv[i];
					save_sol = true;
					break;
				case 'g'://GLPK file data path
					i++;
					glpk_filename = argv[i];
					save_glpk = true;
					solve = false;
					break;
				case 's'://random seed
					i++;
					seed = atoi(argv[i]);
					break;
				case 'a'://alpha parameter
					i++;
					alpha = atof(argv[i]);
					break;
				case 'w'://percentage of nodes to be perturbed in double swap
					i++;
					n_swaps = atof(argv[i]);
					break;
				case 'i'://maximum number of iterations of IVND procedure
					i++;
					 max_iter_vnd = atoi(argv[i]);
					break;
				case 'b'://maximum number of iterations without improvement
					i++;
					max_iter_imp = atoi(argv[i]);
					break;
				case 'r':
					realw_inst = true;
					break;
				case 'v':
					i++;
					for(unsigned int l=0, order=0;l<strlen(argv[i]);l++){
						switch(argv[i][l]){
							case 's':
								ls_index[order]=3;
								order++;
								break;
							case '2':
								ls_index[order]=0;
								order++;
								break;
							case 'r':
								ls_index[order]=1;
								order++;
								break;
							case 'e':
								ls_index[order]=2;
								order++;
								break;
							default:
								printf("Neighborhood not defined %c\n", argv[i][l]);
								exit(-1);
						}
					}
					break;
				default:
					parameterNotDefined(argv[i]);
					exit(-1);
			}
		}else{
			parameterNotDefined(argv[i]);
			exit(-1);
		}
	}
	
	if(!filename_read){
		printf("**Instance file path was not informed, can't proceed.\n");
		usage(argv);
		exit(-1);
	}
}

void parameterNotDefined(string par){
	printf("Parameter %s not defined.\n", par.c_str());
}

void usage(char **argv){
	printf("Usage:\n%s -f <instance file path> \n\t[-o <solution file path>]\n\t[-g <GLPK data file path>]\n\t[-a <alpha parameter>]\n\t[-w <percentage of nodes perturbed>]\n\t[-b num. iterations without improvement]\n\t[-i num. iterations VND]\n\t[-v <VND local search order>]\n\t[-s <seed>]\n", argv[0]);
}
