#include "../include/datafile.h"
#include "../include/constants.h"
#include <cstdio>
#include <iostream>
#include <math.h>
#include <ctime>
#include <time.h>
#include <string.h>

DataFile::DataFile(std::string filename){
	this->filename = filename;
	this->n_pickups = 0;
}

/*Calculates the euclidian distance between two points (x1, y1) and (x2, y2)*/
double euclidianDistance(double x1, double y1, double x2, double y2){
	double x = x1 - x2;
	double y = y1 - y2;

	double t = sqrt(pow(x, 2)+pow(y,2));
	return t;
}

/*Given the path to the instance return the instance name*/
std::string instanceName(std::string filename){
	int i = filename.size()-1;
	int k = i;
	while(k >= 0 && filename[k] != '.')
		k--;
	i = k;
	while(i >= 0 && filename[i] != '/')
		i--;

	i++;
	return std::string(filename.c_str()+i, k-i);
}

/*Read the file given in the filename*/
int DataFile::readData(){
	FILE *file = fopen(this->filename.c_str(), "rt");

	if(file == NULL)
		return -1;

	int temp;
	temp = fscanf(file, "%i\t%i\t%i\n", &this->num_vehicles, &this->vehicles_capacity, &temp);

	std::vector< std::pair<int, int> > devs;
	
	/*Reading data of each customer from file*/
	while(!feof(file)){
		int id, x, y, q, etw, ltw, st, p, d;
		temp = fscanf(file, "%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\n", &id, &x, &y, &q, &etw, &ltw, &st, &p, &d);

		if(d > 0){
			this->n_pickups += 1;
			p = d;
			//TODO: is it really necessary store delivery locations in the data file? 
			devs.push_back(std::make_pair(p, id));
		}

		Node n(id, x, y, q, etw, ltw, st, p);
		this->nodes.push_back(n);
	}
	
	/*Build the distance matrix*/
	int msize = this->n_pickups*2+1;
	//TODO: check if INF or 0.0 have any influence when d[i][i]
	this->distance.assign(msize, std::vector<double>(msize, 0.0));
	
	for(int i=0;i<msize;i++){
		for(int j=i+1;j<msize;j++){
			double d = euclidianDistance(this->nodes[i].getPosX(), this->nodes[i].getPosY(), 
					this->nodes[j].getPosX(), this->nodes[j].getPosY());

			distance[i][j] = d;
			distance[j][i] = d;
		}
	}
	
	//TODO: time matrix should be distance/speed
	this->time = this->distance;
	
	fclose(file);

	return 0;
}


/*Read real world Instances data (with double precision position of nodes) given in the filename*/
int DataFile::readRealWorldData(){
	int temp;
	FILE *file = fopen(this->filename.c_str(), "rt");

	if(file == NULL)
		return -1;

	double speed;
	temp = fscanf(file, "%i\t%i\t%lf\n", &this->num_vehicles, &this->vehicles_capacity, &speed); 
	//avoid warning
	temp+=1;

	std::vector< std::pair<int, int> > devs;
	std::vector<double> x_pos;
	std::vector<double> y_pos;
	
	/*Reading data of each customer from file*/
	while(!feof(file)){
		int id, q, st, p, d;
		double x,y, etw, ltw;
		temp = fscanf(file, "%i\t%lf\t%lf\t%i\t%lf\t%lf\t%i\t%i\t%i\n", &id, &x, &y, &q, &etw, &ltw, &st, &p, &d);

		if(d > 0){
			this->n_pickups += 1;
			p = d;
			//TODO: is it really necessary store delivery locations in the data file? 
			devs.push_back(std::make_pair(p, id));
		}

		Node n(id, (int)floor(x), (int)floor(y), q, (int)etw, (int)ltw, st, p);
		x_pos.push_back(x);
		y_pos.push_back(y);
		this->nodes.push_back(n);
	}
	
	/*Build the distance matrix*/
	int msize = this->n_pickups*2+1;
	//TODO: check if INF or 0.0 have any influence when d[i][i]
	this->distance.assign(msize, std::vector<double>(msize, 0.0));
	this->time.assign(msize, std::vector<double>(msize, 0.0));
	for(int i=0;i<msize;i++){
		for(int j=i+1;j<msize;j++){
			double d = euclidianDistance(x_pos[i], y_pos[i], 
					x_pos[j], y_pos[j]);

			distance[i][j] = d;
			distance[j][i] = d;
			//printf("d[%i][%i] = %.2f\n", i, j, d);
			
			this->time[i][j] = d/speed;
			this->time[j][i] = d/speed;
			//printf("t[%i][%i] = %.2f s = %.2f\n", i, j, d/speed, speed);
		}
	}
	
	//TODO: time matrix should be distance/speed
	//this->time = this->distance;
	
	fclose(file);

	return 0;
}


/*Return the number of pickup requests in this problem*/
int DataFile::getNumPickups(){
	return this->n_pickups;
}

/*Returns the capacity of the vehicles in the problem (homogeneous fleet)*/
int DataFile::getVehiclesCapacity(){
	return this->vehicles_capacity;
}

/*Returns the total number of vehicles in the problem*/
int DataFile::getNumVehicles(){
	return this->num_vehicles;
}

/*Returns the vector of node in the problem*/
std::vector<Node> DataFile::getNodes(){
	return this->nodes;
}

int DataFile::getNumNodes(){
	return (int)this->nodes.size();
}

/*Returns the ith node in the problem*/
Node& DataFile::getNode(int i){
	return this->nodes[i];
}

/*Returns the time to go from customer i to j*/
double DataFile::getTime(int i, int j){
	return this->time[i][j];
}

/*Returns the distance to go from customer i to j*/
double DataFile::getDistance(int i, int j){
	return this->distance[i][j];
}

/*Returns the file name read*/
std::string DataFile::getFilename(){
	return this->filename;
}

struct tm getCurrentTime(){
	time_t t = time(NULL);
	return *localtime(&t);
}

/*Write solution to file*/
int DataFile::writeSolutionFile(std::string sfilename, Solution s){
	/*get instance name*/
	std::string iname = instanceName(this->filename);
	FILE *sfile = fopen(sfilename.c_str(), "wt");

	if(sfile == NULL)
		return -1;	

	fprintf(sfile, "Instance name : %s\n", iname.c_str());
	fprintf(sfile, "Authors       : %s\n", AUTHOR_NAME);
	struct tm localTime = getCurrentTime();
	fprintf(sfile, "Date          : %i.%i.%i\n", localTime.tm_year + 1900, localTime.tm_mon + 1, localTime.tm_mday);
	fprintf(sfile, "Reference     : TCC\n");
	fprintf(sfile, "Solution\n");

	for(int r=0;r<s.getNumRoutes();r++){
		fprintf(sfile, "Route %i :", r+1);
		int node = s.getRoute(r).getForward(0);
		while(node != 0){
			fprintf(sfile, " %i", node);
			node = s.getRoute(r).getForward(node);
		}
		fprintf(sfile, "\n");
	}

	fclose(sfile);

	return 0;	
}

/*Read solution file and return a solution*/
int DataFile::readSolutionFile(std::string sfilename, Solution *s){
	
	FILE *sfile = fopen(sfilename.c_str(), "rt");

	if(sfile == NULL)
		return -1;	

	char line[1000];
	char *temp; //to supress warnings
	int temp2;
	temp = fgets(line, sizeof(line), sfile); //instance name
	temp = fgets(line, sizeof(line), sfile); //author
	temp = fgets(line, sizeof(line), sfile); //date
	temp = fgets(line, sizeof(line), sfile); //reference
	temp = fgets(line, sizeof(line), sfile); //solution
	temp += 1;
	temp2+=1;
	
	while(!feof(sfile)){
		temp = fgets(line, sizeof(line), sfile);
		printf("LINE = %s\n", line);
		temp2 = fscanf(sfile, "\n");
		printf("One Route..\n");
		std::vector<std::string>	split;
		int line_len = strlen(line);
		for(int j=0;j<line_len;j++){
			int k = j;
			while(k < line_len && line[k] != ' ')
				k++;
			
			std::string column(line+j, k-j);
			split.push_back(column);
			j=k;
		}
		
		Route route(this->getNumNodes());
		int i=3;
		int b = 0;
		double rtime_n = 0.0;
		while(i < (int)split.size()){
			int node = atoi(split[i].c_str());
			printf("Adding %i\n", node);
			rtime_n += this->time[b][node];
			route.setReachTime(node, rtime_n);
			rtime_n = std::max(rtime_n, (double)nodes[node].getETW()) + nodes[node].getST();
			route.insertNode(b, node);
			route.updateDistance(this->distance[b][node]);
			b = node;
			i++;
		}
		
		route.updateDistance(this->distance[b][0]);

		s->addRoute(route);
	}

	fclose(sfile);

	return 0;	
}


/*Write the data read to the GLPK data format, to be used by a LP formulation*/
int DataFile::writeAsGLPKData(std::string gfilename){
	FILE *file = fopen(gfilename.c_str(), "wt");

	if(file == NULL)
		return -1;

	fprintf(file, "data;\n");
	fprintf(file, "param n := %i;\n", this->n_pickups);
	fprintf(file, "param Kmax := %i;\n", this->num_vehicles);
	fprintf(file, "param Q := %i;\n", this->vehicles_capacity);

	/*Generate pickup set*/
	fprintf(file, "set P := ");
	for(unsigned int i=0;i<this->nodes.size();i++){
		if(nodes[i].getType() == PICKUP_NODE)
			fprintf(file, "%i ", i);
	}
	fprintf(file, ";\n");

	/*Generate delivery set*/
	fprintf(file, "set R := ");
	for(unsigned int i=0;i<this->nodes.size();i++){
		if(nodes[i].getType() == DELIVERY_NODE)
			fprintf(file, "%i ", i);
	}
	fprintf(file, ";\n");

	/*Generate the delivery vector (ith position is the delivery node of pickup node i)*/
	fprintf(file, "param dev := \n");
	for(unsigned int i=0;i<this->nodes.size();i++){
		if(nodes[i].getType() == PICKUP_NODE)
			fprintf(file, "%i %i\n", i, nodes[i].getPair());
	}
	fprintf(file, ";\n");

	/*Generate the time matrix*/
	fprintf(file, "param T : ");
	for(unsigned int i=0;i<this->nodes.size()+1;i++){
		fprintf(file, "%i ", i);
	}
	fprintf(file, ":=\n ");
	//TODO: check generation of T and D matrix
	for(unsigned int i=0;i<this->nodes.size()+1;i++){
		fprintf(file, "%i ", i);
		for(unsigned int j=0;j<this->nodes.size()+1;j++){
			if(i == this->nodes.size() && j == this->nodes.size())
				fprintf(file, "%.3f ", this->time[0][0]);
			else if(i == this->nodes.size())
				fprintf(file, "%.3f ", this->time[0][j]);
			else if(j == this->nodes.size())
				fprintf(file, "%.3f ", this->time[i][0]);
			else
				fprintf(file, "%.3f ", this->time[i][j]);
		}
		fprintf(file, "\n");
	}
	fprintf(file, ";\n");

	/*Generate the distance matrix*/
	fprintf(file, "param D : ");
	for(unsigned int i=0;i<this->nodes.size()+1;i++){
		fprintf(file, "%i ", i);
	}
	fprintf(file, ":=\n ");
	for(unsigned int i=0;i<this->nodes.size()+1;i++){
		fprintf(file, "%i ", i);
		for(unsigned int j=0;j<this->nodes.size()+1;j++){
			if(i == this->nodes.size() && j == this->nodes.size())
				fprintf(file, "%.3f ", this->distance[0][0]);
			else if(i == this->nodes.size())
				fprintf(file, "%.3f ", this->distance[0][j]);
			else if(j == this->nodes.size())
				fprintf(file, "%.3f ", this->distance[i][0]);
			else
				fprintf(file, "%.3f ", this->distance[i][j]);
		}
		fprintf(file, "\n");
	}
	fprintf(file, ";\n");
	
	fprintf(file, "param c := 1;\n ");

	//TODO: calculate delta
	int delta = 10000;
	fprintf(file, "param delta := %i;\n ", delta);

	/*Generate demand of each node*/
	fprintf(file, "param O := \n");
	for(unsigned int i=0;i<this->nodes.size()+1;i++){
		if(i==this->nodes.size())
			fprintf(file, "%i %i\n", i, nodes[0].getDemand());
		else
			fprintf(file, "%i %i\n", i, nodes[i].getDemand());
	}
	fprintf(file, ";\n");

	/*Generate service time of each node*/
	fprintf(file, "param S := \n");
	for(unsigned int i=0;i<this->nodes.size()+1;i++){
		if(i==this->nodes.size())
			fprintf(file, "%i %i\n", i, nodes[0].getST());
		else
			fprintf(file, "%i %i\n", i, nodes[i].getST());
	}
	fprintf(file, ";\n");

	/*Generate earliest set of time windows of each node*/
	fprintf(file, "param A := \n");
	for(unsigned int i=0;i<this->nodes.size()+1;i++){
		if(i==this->nodes.size())
			fprintf(file, "%i %i\n", i, nodes[0].getETW());
		else
			fprintf(file, "%i %i\n", i, nodes[i].getETW());
	}
	fprintf(file, ";\n");

	/*Generate demand of each node*/
	fprintf(file, "param B := \n");
	for(unsigned int i=0;i<this->nodes.size()+1;i++){
		if(i==this->nodes.size())
			fprintf(file, "%i %i\n", i, nodes[0].getLTW());
		else
			fprintf(file, "%i %i\n", i, nodes[i].getLTW());
	}
	fprintf(file, ";\n");
	
	/*Big integer constant*/
	fprintf(file, "param W := %i;\n", INF);
	
	fprintf(file, "\n\nend;\n");

	return 0;
}


/*Write the data read to the GLPK data format, to be used by a LP formulation*/
int DataFile::tspdptw(std::string tfilename){
	FILE *file = fopen(tfilename.c_str(), "wt");

	if(file == NULL)
		return -1;

	fprintf(file, "data;\n");
	fprintf(file, "param n := %i;\n", this->n_pickups);
	fprintf(file, "param Q := %i;\n", this->vehicles_capacity);

	/*Generate pickup set*/
	fprintf(file, "set P := ");
	for(unsigned int i=0;i<this->nodes.size();i++){
		if(nodes[i].getType() == PICKUP_NODE)
			fprintf(file, "%i ", i);
	}
	fprintf(file, ";\n");

	/*Generate delivery set*/
	fprintf(file, "set R := ");
	for(unsigned int i=0;i<this->nodes.size();i++){
		if(nodes[i].getType() == DELIVERY_NODE)
			fprintf(file, "%i ", i);
	}
	fprintf(file, ";\n");

	/*Generate the delivery vector (ith position is the delivery node of pickup node i)*/
	fprintf(file, "param dev := \n");
	for(unsigned int i=0;i<this->nodes.size();i++){
		if(nodes[i].getType() == PICKUP_NODE)
			fprintf(file, "%i %i\n", i, nodes[i].getPair());
	}
	fprintf(file, ";\n");

	/*Generate the time matrix*/
	fprintf(file, "param T : ");
	for(unsigned int i=0;i<this->nodes.size()+1;i++){
		fprintf(file, "%i ", i);
	}
	fprintf(file, ":=\n ");
	//TODO: check generation of T and D matrix
	for(unsigned int i=0;i<this->nodes.size()+1;i++){
		fprintf(file, "%i ", i);
		for(unsigned int j=0;j<this->nodes.size()+1;j++){
			if(i == this->nodes.size() && j == this->nodes.size())
				fprintf(file, "%.3f ", this->time[0][0]);
			else if(i == this->nodes.size())
				fprintf(file, "%.3f ", this->time[0][j]);
			else if(j == this->nodes.size())
				fprintf(file, "%.3f ", this->time[i][0]);
			else
				fprintf(file, "%.3f ", this->time[i][j]);
		}
		fprintf(file, "\n");
	}
	fprintf(file, ";\n");

	/*Generate the distance matrix*/
	fprintf(file, "param D : ");
	for(unsigned int i=0;i<this->nodes.size()+1;i++){
		fprintf(file, "%i ", i);
	}
	fprintf(file, ":=\n ");
	for(unsigned int i=0;i<this->nodes.size()+1;i++){
		fprintf(file, "%i ", i);
		for(unsigned int j=0;j<this->nodes.size()+1;j++){
			if(i == this->nodes.size() && j == this->nodes.size())
				fprintf(file, "%.3f ", this->distance[0][0]);
			else if(i == this->nodes.size())
				fprintf(file, "%.3f ", this->distance[0][j]);
			else if(j == this->nodes.size())
				fprintf(file, "%.3f ", this->distance[i][0]);
			else
				fprintf(file, "%.3f ", this->distance[i][j]);
		}
		fprintf(file, "\n");
	}
	fprintf(file, ";\n");
	
	fprintf(file, "param c := 1;\n ");

	//TODO: calculate delta
	int delta = 10000;
	fprintf(file, "param delta := %i;\n ", delta);

	/*Generate demand of each node*/
	fprintf(file, "param O := \n");
	for(unsigned int i=0;i<this->nodes.size()+1;i++){
		if(i==this->nodes.size())
			fprintf(file, "%i %i\n", i, nodes[0].getDemand());
		else
			fprintf(file, "%i %i\n", i, nodes[i].getDemand());
	}
	fprintf(file, ";\n");

	/*Generate service time of each node*/
	fprintf(file, "param S := \n");
	for(unsigned int i=0;i<this->nodes.size()+1;i++){
		if(i==this->nodes.size())
			fprintf(file, "%i %i\n", i, nodes[0].getST());
		else
			fprintf(file, "%i %i\n", i, nodes[i].getST());
	}
	fprintf(file, ";\n");

	/*Generate earliest set of time windows of each node*/
	fprintf(file, "param A := \n");
	for(unsigned int i=0;i<this->nodes.size()+1;i++){
		if(i==this->nodes.size())
			fprintf(file, "%i %i\n", i, nodes[0].getETW());
		else
			fprintf(file, "%i %i\n", i, nodes[i].getETW());
	}
	fprintf(file, ";\n");

	/*Generate demand of each node*/
	fprintf(file, "param B := \n");
	for(unsigned int i=0;i<this->nodes.size()+1;i++){
		if(i==this->nodes.size())
			fprintf(file, "%i %i\n", i, nodes[0].getLTW());
		else
			fprintf(file, "%i %i\n", i, nodes[i].getLTW());
	}
	fprintf(file, ";\n");
	
	/*Big integer constant*/
	fprintf(file, "param W := %i;\n", INF);
	
	fprintf(file, "\n\nend;\n");

	return 0;
}


void DataFile::generateOneRouteInstance(Solution s){

	for(int r=0;r<s.getNumRoutes();r++){
		char filename[80];
		char mapname[80];
		sprintf(filename, "inst_%i.txt", r);
		sprintf(mapname, "map_%i.txt", r);
		FILE *fin = fopen(filename, "wt");
		FILE *min = fopen(mapname, "wt");
		Route route = s.getRoute(r);
		int node=route.getForward(0);
		
		std::vector<int> mapping(this->nodes.size(), -1);
		int i=1;
		do{
			mapping[node] = i;
			fprintf(min, "%i - %i\n", node, i);
			i++;
			node=route.getForward(node);
		}while(node != 0);
		fprintf(min, "Route cost: %.2f\n", route.getDistance());
		fprintf(fin, "1 %i 1\n", this->vehicles_capacity);
		fprintf(fin, "0\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\n", nodes[0].getPosX(), nodes[0].getPosY(), 0, nodes[0].getETW(), nodes[0].getLTW(), nodes[0].getST(), 0, 0);		
		
		node=route.getForward(0);
		i=1;
		do{
			fprintf(fin, "%i\t%i\t%i\t%i\t%i\t%i\t%i\t", mapping[node], nodes[node].getPosX(), nodes[node].getPosY(), nodes[node].getDemand(), nodes[node].getETW(), nodes[node].getLTW(), nodes[node].getST());
			if(nodes[node].getType() == PICKUP_NODE){
				fprintf(fin, "0\t%i\n", mapping[nodes[node].getPair()]);
			}else{
				fprintf(fin, "%i\t0\n", mapping[nodes[node].getPair()]);
			}
			node=route.getForward(node);
		}while(node != 0);
		
		fclose(fin);
		fclose(min);
	}
}


/*Prints all the data read*/
void DataFile::print(){
	printf("Filename: %s\n", this->filename.c_str());
	printf("K = %i | Q = %i | R = %i\n ", this->num_vehicles, this->vehicles_capacity, this->n_pickups);
	
	for(unsigned int i=0;i<this->nodes.size();i++){
		printf("%i\t%i %i\t%i\t%i %i\t%i\t", this->nodes[i].getId(), this->nodes[i].getPosX(), this->nodes[i].getPosY(), this->nodes[i].getDemand(), this->nodes[i].getETW(), this->nodes[i].getLTW(), this->nodes[i].getST());
		if(this->nodes[i].getType() == PICKUP_NODE || this->nodes[i].getType() == DEPOT_NODE)
			printf("0 %i\n", this->nodes[i].getPair());
		else
			printf("%i 0\n", this->nodes[i].getPair());
	}	 
}
