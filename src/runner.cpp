#include "../include/datafile.h"
#include "../include/solution.h"
#include "../include/solutionconstructor.h"
#include "../include/localsearch.h"
#include "../include/perturbation.h"
#include "../include/ils.h"
#include "../include/validator.h"
#include <dirent.h>
#include <string>
#include <iostream>
#include <cstdio>
#include <cmath>
#include <chrono>
#include <time.h>

using namespace std;

string direc_path; //path to directory with instances
string csv_filename; //name of the CSV output file
int seed = 1; //seed for random number generation (Default = 1)
bool realw_inst = false;

int max_iter_imp = 82;
int max_iter_vnd = 98;
double alpha = 0.288;
double n_swaps = 0.266;
int ls_index[4] = {3,1,2,0};

void parameterNotDefined(string par);
void usage(char **argv);
void readParameter(int argc, char **argv);
std::string instanceName2(std::string filename);
int validateSolution(DataFile data, Solution s);

#define CSV_HEADER "Instance;#C;Try;Seed;#V(i);Distance(i);t(s)(i);#V(b);Distance(b);t(s)(t)\n"

int main(int argc, char **argv){
	readParameter(argc, argv);
	
	FILE *csvf = fopen(csv_filename.c_str(), "wt");
	if(csvf == NULL){
		printf("Could not create CSV file %s\n", csv_filename.c_str());
		return -1;
	}
	fprintf(csvf, "%s", CSV_HEADER);
		
	DIR *dir;
	struct dirent *ent;
	int ninf = 0;
	int ninst = 0;
	if ((dir = opendir (direc_path.c_str())) != NULL) {
  		/*get all files from directory*/
		while ((ent = readdir (dir)) != NULL) {
			ninst++;
			string instname = direc_path+ent->d_name;
			if(instname == direc_path+".."||instname == direc_path+".") continue;
			printf("%i === opening %s ===\n", ninst, instname.c_str());
			
			DataFile data(instname);
			if(realw_inst){
				if(data.readRealWorldData() == -1){
					printf("Error reading file \'%s\'\n'", ent->d_name);
					fprintf(csvf, "%s;--;--;--;--;--;--;--;--;--\n", instanceName2(ent->d_name).c_str());
					continue;
				}
			}else{
    			if(data.readData() == -1){
					printf("Error reading file \'%s\'\n'", ent->d_name);
					fprintf(csvf, "%s;--;--;--;--;--;--;--;--;--\n", instanceName2(ent->d_name).c_str());
					continue;
				}
			}
			
			time_t theTime = time(NULL);
			struct tm *aTime = localtime(&theTime);

			int day = aTime->tm_mday;
			int month = aTime->tm_mon + 1; // Month is 0 – 11, add 1 to get a jan-dec 1-12 concept
			int year = aTime->tm_year + 1900; // Year is # years since 1900
			int hour=aTime->tm_hour;
			int min=aTime->tm_min;
			printf("%i/%i/%i - %i:%i:00 === Solving instance %s ===\n", day, month, year, hour, min, instanceName2(ent->d_name).c_str());
			
			std::chrono::time_point<std::chrono::system_clock> start_t, end_t;
			int seed = 1;
			int trial = 1;
			
			for(trial=0;trial<10;trial++){
				seed = time(NULL);
				
				//Get time to generate initial solution
				start_t = std::chrono::system_clock::now();
				SolutionConstructor sc(data);
				Solution bs = sc.insertionHeuristic();
				end_t = std::chrono::system_clock::now();
				std::chrono::duration<double> init_el_sec = end_t-start_t;
				
				//Run ILS and get its time and solution
				start_t = std::chrono::system_clock::now();
				ILS ils(data, max_iter_imp, max_iter_vnd, n_swaps, alpha, seed, ls_index);
				bs = ils.run();
				end_t = std::chrono::system_clock::now();

				Solution i_s = ils.getInitialSolution();
			
				std::chrono::duration<double> el_sec = end_t-start_t;
			
				int is_nv = i_s.getNumRoutes();
				double is_d = i_s.getDistance();
				int is_res = Validator::validateSolution(data, i_s);
				int bs_nv = bs.getNumRoutes();
				double bs_d = bs.getDistance();
				int bs_res = Validator::validateSolution(data, bs);
				
				//check if solution is valid
				if(is_res != 0 || bs_res != 0){
					ninf++;
					printf("Result i = %i\n", is_res);
					printf("Result b = %i\n", bs_res);
					is_nv = -1;
					is_d = -1;
					bs_nv = -1;
					bs_d = -1;
					return 0;
				}
				
				theTime = time(NULL);
				aTime = localtime(&theTime);
				day = aTime->tm_mday;
				month = aTime->tm_mon + 1; // Month is 0 – 11, add 1 to get a jan-dec 1-12 concept
				year = aTime->tm_year + 1900; // Year is # years since 1900
				hour=aTime->tm_hour;
				min=aTime->tm_min;
				printf("%i - Best Found :: D = %.2f | #V = %i (Ell. time = %.2f s) : %i/%i/%i - %i:%i\n", trial, bs_d, bs_nv, el_sec.count(), day, month, year, hour, min);
				fprintf(csvf, "%s;%i;%i;%i;%i;%.2f;%.2f;%i;%.2f;%.2f\n", instanceName2(ent->d_name).c_str(), data.getNumNodes(), trial, seed, is_nv, is_d, init_el_sec.count(), bs_nv, bs_d, el_sec.count());
			}
  		}
  		/*Close directory file*/
		closedir(dir);
	}else {
  		/* could not open directory */
  		printf("Error opening directory %s\n", direc_path.c_str());
  		return -1;
  	}
  	fclose(csvf);
  	printf("INF = %i\n", ninf);
  	printf("Success!\n");
  	return 0;
}


std::string instanceName2(std::string filename){
	int i = filename.size()-1;
	int k = i;
	while(k >= 0 && filename[k] != '.')
		k--;
	i = k;
	while(i >= 0 && filename[i] != '/')
		i--;

	i++;
	return std::string(filename.c_str()+i, k-i);
}

/*Read the input parameters*/
void readParameter(int argc, char **argv){

	if(argc < 3){
		usage(argv);
		exit(-1);
	}
	
	bool direc_read = false;
	bool csvf_read = false;
	
	for(int i=1;i<argc;i++){
		if(argv[i][0] == '-'){
			switch(argv[i][1]){
				case 'd'://directory file path
					i++;
					direc_path = argv[i];
					direc_read = true;
					break;
				case 'o'://csv file name
					i++;
					csv_filename = argv[i];
					csvf_read = true;
					break;
				case 'r':
					realw_inst = true;
					break;
				default:
					parameterNotDefined(argv[i]);
					exit(-1);
			}
		}else{
			parameterNotDefined(argv[i]);
			exit(-1);
		}
	}
	
	if(!direc_read){
		printf("**Directory file path was not informed, can't proceed.\n");
		usage(argv);
		exit(-1);
	}
	if(!csvf_read){
		printf("**CSV output file name was not informed, can't proceed.\n");
		usage(argv);
		exit(-1);
	}
}

void parameterNotDefined(string par){
	printf("Parameter %s not defined.\n", par.c_str());
}

void usage(char **argv){
	printf("Usage:\n%s -d [directory file path] -o [csv file name]\n", argv[0]);
}
