#include "../include/solutionconstructor.h"
#include "../include/constants.h"
#include "../include/errors.h"
#include <stdexcept>
#include <vector>
#include <cstdio>
#include <algorithm>

SolutionConstructor::SolutionConstructor(DataFile data) : data(""){
	this->data = data;
}

/*Run an adapted version of the insertion heuristic (by Solomon)*/
Solution SolutionConstructor::insertionHeuristic(){
	Solution s; //solution to be returned
	std::vector<bool> routed(this->data.getNumNodes(), false); //indicates wheter a node was already routed
	int req_attended = 0;	//indicates how many requests have been attended
	
	/*While there are requests to be attended and vehicles to do so*/
	while(req_attended < this->data.getNumPickups() && s.getNumRoutes() < this->data.getNumVehicles()){
		Route route(this->data.getNumNodes());
		int u = getRouteFirstNode(routed);	
		int back_u = 0;
		int back_v = u;
		//while there are PD pairs to be inserted in this route
		while(u != NO_NODE){
			req_attended++;
			//Insert PD pair in route
			route = insertInRoute(route, back_u, u); //pickup node
			route = insertInRoute(route, back_v, this->data.getNode(u).getPair()); //delivery node
			//mark it as routed
			routed[u] = true;
			routed[this->data.getNode(u).getPair()] = true;
			//Get another PD pair to insert
			PairMovementTuple bestPD = getBestPDPair(route, routed);
			back_u = bestPD.back_u;
			u = bestPD.u;
			back_v = bestPD.back_v;
		}
		//if the no node could be inserted in this route, it is an error
		if(route.getSize() == 0)
			break;
		//insert the new route in solution	
		s.addRoute(route);
	}
	
	//if not all requests were attended, there is an error
	if(req_attended < this->data.getNumPickups()){
		throw std::runtime_error(NO_FEASIBLE_SOL_MSG);
	}
	
	//printf("s.dist = %.2f\ns.size = %i\n", s.getDistance(), s.getNumRoutes());
	//return the constructed feasible solution
	return s;
}


/*Check if inserting u after node i in route is a valid movement*/
bool SolutionConstructor::isValidInsertion(Route route, int i, int u, double rtime_i){
	bool valid = false;
	double departure_time_i = 0.0; //default case is when i == 0
	
	if(i != 0)
		departure_time_i = std::max(rtime_i, (double)this->data.getNode(i).getETW()) + this->data.getNode(i).getST(); //time to leave node i
		
	double rtime_u = departure_time_i + this->data.getTime(i, u); //time to reach node u from i
	double start_serv_u = std::max(rtime_u, (double)this->data.getNode(u).getETW()); //time for service to start at node u
	
	//If service start at node u before its latest time window, and if the vehicle is not overloaded
	if(start_serv_u <= this->data.getNode(u).getLTW() && route.getAccCapacity(i)+this->data.getNode(u).getDemand() <= this->data.getVehiclesCapacity()){
		double departure_time_u = start_serv_u + this->data.getNode(u).getST(); //time to leave node u
		int node = route.getForward(i); //future forward of node u
		double rtime_n = departure_time_u + this->data.getTime(u, node); //time to reach node
		valid = true;
		//Check each following time window so that there is no violation of it
		do{
			double departure_time_n = std::max(rtime_n, (double)this->data.getNode(node).getETW());
			if(departure_time_n > this->data.getNode(node).getLTW()){
				valid = false;
				break;
			}else if(departure_time_n == std::max(route.getReachTime(node), (double)this->data.getNode(node).getETW())){
				//if the time to start service at node is the same as before, the rest of the route will have no modifications
				break;
			}
			departure_time_n +=  this->data.getNode(node).getST();
			rtime_n = departure_time_n + this->data.getTime(node, route.getForward(node));
			node = route.getForward(node);
		}while(node != route.getForward(0));
	}
	
	return valid;
}

/*Get the PD pair that minimizes distance cost when inserting in route*/
PairMovementTuple SolutionConstructor::getBestPDPair(Route route, std::vector<bool> routed){
	PairMovementTuple bp; //best pair for insertion
	
	for(int u=1;u<this->data.getNumNodes();u++){ //u is the node trying to be inserted
		if(routed[u] == true || this->data.getNode(u).getType() == DELIVERY_NODE)
			continue;
		
		int i = 0;// u will be inserted after i in route
		do{
			if(isValidInsertion(route, i, u, route.getReachTime(i))){ //if inserting u after i in route is a valid insertion
				Route or_route = route; //keep original route
				route.insertNode(i, u); //insert node u in route
				int v =  this->data.getNode(u).getPair(); //v is the delivery pair of node u
				int j = u; //v will be inserted after j in route
				double departure_time_i = 0.0; //time to leave node i (default is when i == 0)
				
				if(i != 0)
					departure_time_i = std::max(route.getReachTime(i),(double) this->data.getNode(i).getETW()) + this->data.getNode(i).getST(); //time to leave node i
				double rtime_j = departure_time_i + this->data.getTime(i, j); //time to reach node j (in this case, u)
				
				/*try to insert node v after every node j untill the depot*/
				while(j != 0){
					/*If the vehicle is overloaded at node j, node v can't be inserted AFTER it (solution will be infeasible)'*/
					if(route.getAccCapacity(j)+this->data.getNode(u).getDemand() > this->data.getVehiclesCapacity())
						break;

					if(isValidInsertion(route, j, v, rtime_j)){
						if(bp.u == NO_NODE 
						|| c1(bp.back_u, bp.u, bp.for_u)+c1(bp.back_v, bp.v, bp.for_v) > c1(i, u, route.getForward(u))+c1(j, v, route.getForward(j))){
							/*If the cost of inserting the pair at this position is less than the older one, update it*/
							bp.back_u = i;	
							bp.u = u;
							bp.for_u = route.getForward(u);
							bp.back_v = j;
							bp.v = v;
							bp.for_v = route.getForward(j);
						}
					}
					double departure_time_j = std::max(rtime_j,(double) this->data.getNode(j).getETW()) + this->data.getNode(j).getST();
					rtime_j = departure_time_j + this->data.getTime(j, route.getForward(j));
					j = route.getForward(j);
				}
				//Roll back route to its original state
				route = or_route;
			}
			i = route.getForward(i);
		}while(i != 0);
	}

	return bp;
}

/*Cost type 1 to insert node u between i and j*/
double SolutionConstructor::c1(int i, int u, int j){
	return (this->data.getDistance(i, u) + this->data.getDistance(u, j) - this->data.getDistance(i, j));
}

/*Insert node u after node i in route, updating information accordingly*/
Route SolutionConstructor::insertInRoute(Route route, int i, int u){
	/*set accumulated capacity for node u*/
	route.setAccCapacity(u, route.getAccCapacity(i)+this->data.getNode(u).getDemand());
	
	double departure_time_i = 0.0;//time to leave node i
	if(i != 0)
	 	departure_time_i = std::max(route.getReachTime(i), (double)this->data.getNode(i).getETW()) + this->data.getNode(i).getST();
	
	route.setReachTime(u, departure_time_i + this->data.getTime(i, u)); //time to reach node u
	double departure_time_u = std::max(route.getReachTime(u), (double)this->data.getNode(u).getETW()) + this->data.getNode(u).getST(); //time to leave node u

	int node = route.getForward(i); //future forward of node u
	double rtime_n = departure_time_u + this->data.getTime(u, node); //time to reach node
	//update all reach time and capacity information
	do{
		route.setReachTime(node, rtime_n);
		route.updateAccCapacity(node, this->data.getNode(u).getDemand());

		double departure_time_n = std::max(rtime_n ,(double) this->data.getNode(node).getETW())+this->data.getNode(node).getST();
		rtime_n = departure_time_n + this->data.getTime(node, route.getForward(node));
		node = route.getForward(node);	
	}while(node != route.getForward(0));
	
	//update route distance
	double addDist = this->data.getDistance(i, u)+this->data.getDistance(u, route.getForward(i)) - this->data.getDistance(i, route.getForward(i));	
	route.insertNode(i, u);	
	route.updateDistance(addDist);
		
	return route;
}

/*Get the first node to insert in route*/
int SolutionConstructor::getRouteFirstNode(std::vector<bool> routed){
	int n = NO_NODE;
	double d = INF;
	double t = INF;
	for(int i=1;i<data.getNumNodes();i++){
		if(this->data.getNode(i).getType() == PICKUP_NODE 
		&& routed[i] == false
		&& this->data.getNode(i).getETW() <= t
		&& this->data.getTime(0, i) <= this->data.getNode(i).getLTW() ){
			if(t == this->data.getNode(i).getETW()){
				if(d >= this->data.getDistance(0, i)){
					n = i;
					d = this->data.getDistance(0, i);
				}
			}else{
				n = i;
				t = this->data.getNode(i).getETW();
				d = this->data.getDistance(0, i);
			}
		}
	}
	
	return n;

// Li & Lim implementation (for testing purposes)	
//	int n = NO_NODE;
//	double d = -1*INF;
//	double t = INF;
//	double p = INF;
//	for(int i=1;i<data.getNumNodes();i++){
//		if(this->data.getNode(i).getType() == PICKUP_NODE 
//		&& routed[i] == false
//		&& d < this->data.getDistance(0, i)
//		&& t > this->data.getNode(i).getLTW()
//		&& p > this->data.getNode(i).getLTW()-this->data.getNode(i).getETW() ){
//			d = this->data.getDistance(0, i);
//			t = this->data.getNode(i).getLTW();
//			p = t-this->data.getNode(i).getETW();
//			n = i;
//		}
//	}
//	
//	return n;
	
}
