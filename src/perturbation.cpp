#include "../include/perturbation.h"
#include <set>
#include <vector>
#include "../include/constants.h"


/*Tuple to hold a pair shift between two routes*/
struct PairShiftTuple{
	int back_u;
	int u;
	int back_v;
	int v;
	int r1;
	int r2;
	double cost;
	bool lessr;
	double cc1;
	double cc2;
};


Perturbation::Perturbation(DataFile data, int rseed) : data(""){
	this->data = data;
	this->rseed = rseed;
	this->generator = std::default_random_engine(rseed);
}

Solution Perturbation::doubleSwap(Solution s, int n_swaps){
	if(s.getNumRoutes() < 2){
		return s;
	}

	std::set< std::pair<int, int> > tried; //swaps tried
	std::uniform_int_distribution<int> rand_route(0, s.getNumRoutes()-1); /*Random probability generator*/
	std::uniform_int_distribution<int> rand_node(1, data.getNumNodes()-1);
	int max_tries = this->data.getNumNodes();//std::max(5.0, 0.05*this->data.getNumNodes());
	int tries = 0;
	for(int i=0;i<n_swaps;i++){
		tries++;
		if(tries > max_tries)
			break;
			
		int r1 = rand_route(this->generator);
		int r2 = rand_route(this->generator);
		while(r1 == r2)
			r2 = rand_route(this->generator);
		
		Route route1 = s.getRoute(r1);
		Route route2 = s.getRoute(r2);
		
		int u;
		int x;
		
		u = rand_node(this->generator);
		while(!route1.isNodeInRoute(u) || this->data.getNode(u).getType() == DELIVERY_NODE)
			u = rand_node(this->generator);
			
		x = rand_node(this->generator);
		while(!route2.isNodeInRoute(x) || this->data.getNode(x).getType() == DELIVERY_NODE)
			x = rand_node(this->generator);

		if(tried.count(std::make_pair(u,x)) > 0){
			i--;
			continue;
		}
				
		Route rroute1 = removePairFromRoute(route1, u);
		PairMovementTuple np1 = searchInsertionLocation(rroute1, x);
		if(np1.u == NO_NODE){
			i--;
			continue;
		}
			
		Route rroute2 = removePairFromRoute(route2, x);
		PairMovementTuple np2 = searchInsertionLocation(rroute2, u);
		if(np2.u == NO_NODE){
			i--;
			continue;
		}
			
		rroute1 = insertInRoute(rroute1, np1.back_u, np1.u);
		rroute1 = insertInRoute(rroute1, np1.back_v, np1.v);
		rroute2 = insertInRoute(rroute2, np2.back_u, np2.u);	
		rroute2 = insertInRoute(rroute2, np2.back_v, np2.v);
			
		s.updateRoute(r1, rroute1);
		s.updateRoute(r2, rroute2);		
			
		tried.insert(std::make_pair(u,x));
	}
	
	return s;
}


PairMovementTuple Perturbation::searchInsertionLocation(Route route, int u){
	PairMovementTuple new_pair;
	
	int i=0; //u will be inserted after i in route 2
	do{
		//checks if inserting u after i in route does not violate constraints
		if(isValidInsertion(&route, i, u, route.getReachTime(i))){ 
			route.insertNode(i, u);
			int v = this->data.getNode(u).getPair(); //the delivery pair of pickup node u
			int j = u; //v will be inserted after j in route
			double departure_time_i = 0.0; //time to leave node i (default is when i == 0)
				
			if(i != 0)
				departure_time_i = std::max(route.getReachTime(i),(double) this->data.getNode(i).getETW()) + this->data.getNode(i).getST(); //time to leave node i
			double rtime_j = departure_time_i + this->data.getTime(i, j); //time to reach node j (in this case, u)
				
			/*try to insert node v after every node j untill the depot*/
			while(j != 0){
				/*If the vehicle is overloaded at node j, node v can't be inserted AFTER it (solution will be infeasible)'*/
				if(route.getAccCapacity(j)+this->data.getNode(u).getDemand() > this->data.getVehiclesCapacity())
					break;
						
				/*Checks if inserting v after j in route 2 does not violate constraints*/
				if(isValidInsertion(&route, j, v, rtime_j)){
					new_pair.back_u = i;
					new_pair.u = u;
					new_pair.back_v = j;
					new_pair.v = v;
					return new_pair;
				}
				
				double departure_time_j = std::max(rtime_j,(double) this->data.getNode(j).getETW()) + this->data.getNode(j).getST();
				rtime_j = departure_time_j + this->data.getTime(j, route.getForward(j));
				j = route.getForward(j);
			}
			route.removeNode(u);
		}
		
		i = route.getForward(i);
	}while(i != 0);
	
	return new_pair;	
}

/*Insert node u after node i in route, updating information accordingly*/
Route Perturbation::insertInRoute(Route route, int i, int u){
	/*set accumulated capacity for node u*/
	route.setAccCapacity(u, route.getAccCapacity(i)+this->data.getNode(u).getDemand());
	
	double departure_time_i = 0.0;//time to leave node i
	if(i != 0)
	 	departure_time_i = std::max(route.getReachTime(i), (double)this->data.getNode(i).getETW()) + this->data.getNode(i).getST();
	
	route.setReachTime(u, departure_time_i + this->data.getTime(i, u)); //time to reach node u
	double departure_time_u = std::max(route.getReachTime(u), (double)this->data.getNode(u).getETW()) + this->data.getNode(u).getST(); //time to leave node u

	int node = route.getForward(i); //future forward of node u
	double rtime_n = departure_time_u + this->data.getTime(u, node); //time to reach node
	//update all reach time and capacity information
	do{
		route.setReachTime(node, rtime_n);
		route.updateAccCapacity(node, this->data.getNode(u).getDemand());

		double departure_time_n = std::max(rtime_n ,(double) this->data.getNode(node).getETW())+this->data.getNode(node).getST();
		rtime_n = departure_time_n + this->data.getTime(node, route.getForward(node));
		node = route.getForward(node);	
	}while(node != route.getForward(0));
	
	//update route distance
	double addDist = c_in(i, u, route.getForward(i));	
	route.insertNode(i, u);	
	route.updateDistance(addDist);
		
	return route;
}

/*Remove node u and its delivery pair from route, updating information accordingly*/
Route Perturbation::removePairFromRoute(Route route, int u){
		int v = this->data.getNode(u).getPair();
	int i = route.getBackward(u);
	double departure_time_i = 0.0;
	//printf("R - u=%i, bu=%i, fu=%i\n", u, i, route.getForward(u));
	if(i != 0)
	 	departure_time_i = std::max(route.getReachTime(i), (double)this->data.getNode(i).getETW()) + this->data.getNode(i).getST();

	int node = route.getForward(u);
	int add_cap = -this->data.getNode(u).getDemand();
	if(node == v){
		node = route.getForward(node);
		add_cap = 0;
	}
	double rtime_n = departure_time_i + this->data.getTime(i, node); //time to reach node 

	//update all reach time and capacity information
	while(node != route.getForward(0)){
		if(node == v){
			if(route.getBackward(node) == u){
				rtime_n -= this->data.getTime(i, node);
				rtime_n += this->data.getTime(i, route.getForward(node));
			}else{
				rtime_n -= this->data.getTime(route.getBackward(node), node);
				rtime_n += this->data.getTime(route.getBackward(node), route.getForward(node));
			}
			add_cap = 0;
		}else{	
			route.setReachTime(node, rtime_n);
			route.updateAccCapacity(node, add_cap);

			double departure_time_n = std::max(rtime_n ,(double) this->data.getNode(node).getETW())+this->data.getNode(node).getST();
			rtime_n = departure_time_n + this->data.getTime(node, route.getForward(node));
		}
		node = route.getForward(node);	
	}
	
	//update route distance
	double addDist = c_rm(route.getBackward(u), u, route.getForward(u));
	route.removeNode(u);
	addDist += c_rm(route.getBackward(v), v, route.getForward(v));	
	route.removeNode(v);	
	route.updateDistance(addDist);
		
	return route;
}

/*Cost to insert node u between i and j*/
double Perturbation::c_in(int i, int u, int j){
	return (this->data.getDistance(i, u) + this->data.getDistance(u, j) - this->data.getDistance(i, j));
}

/*Cost to remove node u between i and j*/
double Perturbation::c_rm(int i, int u, int j){
	return -(this->data.getDistance(i, u) + this->data.getDistance(u, j) - this->data.getDistance(i, j));
}


/*Check if inserting u after node i in route is a valid movement*/
bool Perturbation::isValidInsertion(Route *route, int i, int u, double rtime_i){
	bool valid = false;
	double departure_time_i = 0.0; //default case is when i == 0
	
	if(i != 0)
		departure_time_i = std::max(rtime_i, (double)this->data.getNode(i).getETW()) + this->data.getNode(i).getST(); //time to leave node i
		
	double rtime_u = departure_time_i + this->data.getTime(i, u); //time to reach node u from i
	double start_serv_u = std::max(rtime_u, (double)this->data.getNode(u).getETW()); //time for service to start at node u
	
	//If service start at node u before its latest time window, and if the vehicle is not overloaded
	if(start_serv_u <= this->data.getNode(u).getLTW() && route->getAccCapacity(i)+this->data.getNode(u).getDemand() <= this->data.getVehiclesCapacity()){
		double departure_time_u = start_serv_u + this->data.getNode(u).getST(); //time to leave node u
		int node = route->getForward(i); //future forward of node u
		double rtime_n = departure_time_u + this->data.getTime(u, node); //time to reach node
		valid = true;
		Node nd = this->data.getNode(node);
		//Check each following time window so that there is no violation of it
		do{
			double departure_time_n = std::max(rtime_n, (double)nd.getETW());
			if(departure_time_n > nd.getLTW()){
				valid = false;
				break;
			}else if(departure_time_n == std::max(route->getReachTime(node), (double)nd.getETW())){
				//if the time to start service at node is the same as before, the rest of the route will have no modifications
				break;
			}
			departure_time_n += nd.getST();
			rtime_n = departure_time_n + this->data.getTime(node, route->getForward(node));
			node = route->getForward(node);
			nd = this->data.getNode(node);
		}while(node != route->getForward(0));
	}
	
	return valid;
}

