#include "../include/solution.h"	
#include "../include/errors.h"
#include <stdexcept>

Solution::Solution(){
	this->distance = 0;
}

/*Return the rth route in this solution*/
Route Solution::getRoute(int r){
	if(r >= this->getNumRoutes() || r < 0)
		throw std::out_of_range(ROUTE_INDX_OFR_MSG);
	
	return this->routes[r];
}

/*Return the distance of this solution*/
double Solution::getDistance(){
	return this->distance;
}

/*Return the number of routes in this solution*/  
int Solution::getNumRoutes(){
	return this->routes.size();
}

/*Add a route to the solution*/
void Solution::addRoute(Route new_route){
	if(new_route.getSize() == 0)
		throw std::invalid_argument(ROUTE_NO_NODE_MSG);

	this->routes.push_back(new_route);
	this->distance += new_route.getDistance();
}

/*Updates the rth route replacing it by new_route. Updates total solution distance accordingly.*/
void Solution::updateRoute(int r, Route new_route){
	this->distance -= this->routes[r].getDistance();
	this->routes[r] = new_route;
	this->distance += this->routes[r].getDistance();
}

void Solution::removeRoute(int r){
	this->distance -= this->routes[r].getDistance();
	this->routes.erase(this->routes.begin()+r);
}

/*Overload of comparison functions to compare two solutions*/
bool Solution::operator<(Solution& s){
	if(this->getNumRoutes() < s.getNumRoutes())
		return true;
	else if(this->getNumRoutes() == s.getNumRoutes() && this->getDistance() < s.getDistance())
		return true;

	return false;
}

bool Solution::operator<=(Solution& s){
	if(this->getNumRoutes() < s.getNumRoutes())
		return true;
	else if(this->getNumRoutes() == s.getNumRoutes() && this->getDistance() <= s.getDistance())
		return true;
	
	return false;
}

bool Solution::operator>(Solution& s){
	if(this->getNumRoutes() > s.getNumRoutes())
		return true;
	else if(this->getNumRoutes() == s.getNumRoutes() && (this->getDistance() - s.getDistance()) > 0.001)
		return true;

	return false;
}

bool Solution::operator>=(Solution& s){
	if(this->getNumRoutes() > s.getNumRoutes())
		return true;
	else if(this->getNumRoutes() == s.getNumRoutes() && (this->getDistance() - s.getDistance()) > 0.001)
		return true;
	else if(this->getNumRoutes() == s.getNumRoutes() && abs(this->getDistance() - s.getDistance()) <= 0.00000001)
		return true;

	return false;
}

bool Solution::operator==(Solution& s){
	if(this->getNumRoutes() != s.getNumRoutes() || this->getDistance() != s.getDistance())
		return false;
	
	std::vector<bool> routeChecked(s.getNumRoutes(), false);

	for(int r=0;r < this->getNumRoutes();r++){
		int k = 0;
		while(k < s.getNumRoutes()){
			if(routeChecked[k]){
				k++; continue;			
			}
			bool equalr = true;
			if( this->getRoute(r).getSize() == s.getRoute(k).getSize() && this->getRoute(r).getDistance() == s.getRoute(k).getDistance()){
				int node = 0;
				for(int i=0;i < this->getRoute(r).getSize();i++){
					if(this->getRoute(r).getForward(node) != s.getRoute(k).getForward(node)){
						equalr = false;
						break;					
					}
					node = this->getRoute(r).getForward(node);
				}
			}
			if(equalr){
				routeChecked[k] = true;
				break;			
			}
			k++;
		}
		if(k == s.getNumRoutes())
			return false;
		
	}

	return false;
}

void Solution::setRoute(int r, Route route){
	this->routes[r] = route;
}
