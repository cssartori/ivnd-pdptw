#include "../include/ils.h"
#include "../include/solutionconstructor.h"
#include "../include/localsearch.h"
#include "../include/perturbation.h"
#include "../include/vnd.h"
#include <chrono>

int default_ls_index[4] = {3,1,2,0};

ILS::ILS(DataFile data, int max_iter_imp, int max_iter_vnd, double n_swaps, double alpha, int rseed, int *ls_index) : data(""){
	this->data = data;
	this->rseed = rseed;
	this->iterations = 0;
	this->max_iter_vnd = max_iter_vnd;
	this->max_iter_imp = max_iter_imp;
	this->n_swaps = n_swaps;
	this->alpha = alpha;
	
	if(ls_index == NULL)
		this->ls_index = default_ls_index;
	else
		this->ls_index = ls_index;
}

Solution ILS::run(){
	SolutionConstructor sc(data);
	this->initial_solution = sc.insertionHeuristic();
	LocalSearch ls(data);
	Perturbation pb(data, this->rseed);
		
	std::default_random_engine generator(this->rseed);
	std::uniform_real_distribution<double> rand_p(0,1);
	
	VND vnd(&data, &ls);
	
	Solution b_s =this->initial_solution;
	Solution ts = b_s;
	
	//printf("0 :: IS = %.2f | #V = %i\n", initial_solution.getDistance(), initial_solution.getNumRoutes());
	int i_no_b = 0;
	int iter = 0;
	int dswap_size = this->n_swaps*this->data.getNumNodes();
	double p = 0.0;	
	
	while(i_no_b < max_iter_imp){
		iter++;
		
		//calculate all the Forward Time Slacks to be used in the VND
		ts = ls.calculateAllFTimeSlack(ts);
		Solution tb_s = vnd.run(ts, this->max_iter_vnd, this->ls_index);		
		
		if(tb_s >= b_s){
			i_no_b++;
			p = rand_p(generator);
			if( p > this->alpha*((double)i_no_b/(double)max_iter_imp)){
				tb_s = b_s;
			}		
		}else{
			i_no_b = 0;
			b_s = tb_s;
			//printf("%i - BS = %.2f | #V = %i\n", iter, tb_s.getDistance(), tb_s.getNumRoutes());
		}

		tb_s = pb.doubleSwap(tb_s, dswap_size);
		
		ts = tb_s;
	}
	
	this->best_solution = b_s;
	//printf("%i ** BS = %.2f | #V = %i\n", iter, b_s.getDistance(), b_s.getNumRoutes());
	return b_s;			
}

Solution ILS::getInitialSolution(){
	return initial_solution;
}

Solution ILS::getBestSolution(){
	return best_solution;
}


