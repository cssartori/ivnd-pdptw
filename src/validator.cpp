#include "../include/validator.h"
#include <vector>
#include <cmath>
using namespace std;

/*Validate the solution according to the PDPTW constraints. Return 0 when valid*/
int Validator::validateSolution(DataFile data, Solution s){
	double totalDist = 0; //total solution distance

	for(int r=0;r<s.getNumRoutes();r++){
		/*For each route r in solution s*/
		Route route = s.getRoute(r);

		int node = 0; //first node is the depot (0)
		double rdist = 0; //total distance of route r
		double rtime = 0; //total time of route r
		int load = 0; //total load of vehicle in rotue r
		vector<bool> visited(data.getNumNodes(), false); //check if a node was already visited in this route
		
		do{
			//update distance and time
			rdist += data.getDistance(node, route.getForward(node));
			rtime += data.getTime(node, route.getForward(node));
			//get next node in route
			node = route.getForward(node);
			rtime = max(rtime, (double)data.getNode(node).getETW()); 
			
			//if the max time window constraint is violated
			if(data.getNode(node).getLTW() < rtime) 
				return Validator::INVALID_TIME_WINDOW;
			
			//update time with the service time of node
			rtime += data.getNode(node).getST();
			//update load
			load += data.getNode(node).getDemand();

			//if the load is smaller than the necessary to serve this delivery, the load constraint is violated
			if(data.getNode(node).getType() == DELIVERY_NODE && load < data.getNode(node).getDemand()) 
				return Validator::INVALID_DEMAND_SUPPLY;
			//if the pickup pair of this delivery node was not visited yet, the pairing condition is violated
			else if(data.getNode(node).getType() == DELIVERY_NODE && visited[data.getNode(node).getPair()] == false)
				return Validator::INVALID_PAIRING;			
			
			//set this node as visited
			visited[node] = true;
						
			//if the capacity is smaller than 0 or bigger than the maximum the vehicle can carry
			if(load < 0 || load > data.getVehiclesCapacity()) 
				return Validator::VEHICLE_OVERLOADED;
			
		}while(node != 0);
		
		//if the route distance calculated does not match the informed
		if(std::abs((double)(route.getDistance() - rdist)) > 0.001)
			return Validator::INVALID_ROUTE_COST;
		//update the total distance of the solution
		totalDist += rdist;		
	}
	
	//if the solution distance does not match the informed
	if(std::abs((double)(s.getDistance() - totalDist)) > 0.001)
		return Validator::INVALID_SOLUTION_COST;

	return Validator::VALID_SOLUTION;
}
