#ifndef __MOVEMENT_CACHE_H__
#define __MOVEMENT_CACHE_H__

#include "pairmovementtuple.h"
#include <vector>

class MovementCache{
private:
	std::vector< std::vector< std::vector<PairMovementTuple> > > cache_matrix;
	int mod_r1; //route 1 modified
	int mod_r2; //route 2 modified
	int nr;		//cache matrix size (nr x nr)
		
public:
	MovementCache();
	MovementCache(int nr);
	void initCache(int nr);
	void addMovement(int r1, int r2, PairMovementTuple pmt);
	PairMovementTuple getMovement(int r1, int r2, int p=0);	
	std::vector<PairMovementTuple> getAllMovements(int r1, int r2);
	bool hasMovement(int r1, int r2);
	int getModifiedRoute1();
	int getModifiedRoute2();
	bool isRouteModified(int r);
	void setModifiedRoute1(int mod_r1);
	void setModifiedRoute2(int mod_r2);
	void clearCache();
	void clearRoutes(int r1, int r2);

};

#endif //__MOVEMENT_CACHE_H__
