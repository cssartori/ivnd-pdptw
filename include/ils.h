#ifndef __ILS_H__
#define __ILS_H__

#include "datafile.h"
#include "solution.h"
class ILS{
private:
	DataFile data;
	Solution initial_solution;
	Solution best_solution;
	
	int rseed;
	int max_iter_vnd;
	int max_iter_imp;
	double alpha;
	double n_swaps;
	int *ls_index;

	
public:
	ILS(DataFile data, int max_iter_imp=100, int max_iter_vnd=100, double n_swaps=0.1, double alpha=0.0, int rseed=1, int *ls_index=NULL);
	Solution run();
	Solution getInitialSolution();
	Solution getBestSolution();
	int iterations;
};

#endif //__ILS_H__
