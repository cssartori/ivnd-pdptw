#ifndef __PAIR_MOVEMENT_TUPLE_H__
#define __PAIR_MOVEMENT_TUPLE_H__

#include "constants.h"

class PairMovementTuple{
public:
	int back_u; /*Backward of node u*/
	int u;		/*Node u*/
	int for_u;	/*Forward of node u*/

	int back_v;	/*Backward of node v*/
	int v;		/*Node v*/
	int for_v;	/*Forward of node v*/

	int r1;		/*route 1 involved in the movement*/
	int r2;		/*route 2 involved in the movement*/
	
	bool lessr; /*true if the movement leads to fewer routes*/
	int mr;		/*magnitude of the routes (square of the size of the routes)*/
	
	double cost;/*cost of the movement*/

	PairMovementTuple(){
		this->back_u = NO_NODE;
		this->u = NO_NODE;
		this->for_u = NO_NODE;
		this->back_v = NO_NODE;
		this->v = NO_NODE;
		this->for_v = NO_NODE;

		this->r1 = NO_ROUTE;
		this->r2 = NO_ROUTE;

		this->lessr = false;
		this->mr = 0;

		this->cost = INF;
	}
};

#endif //__PAIR_MOVEMENT_TUPLE_H__
