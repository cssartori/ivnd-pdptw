#ifndef __VND_H__
#define __VND_H__

#include "localsearch.h"
#include "datafile.h"
#include "solution.h"

class VND{
private:
	DataFile *data;
	LocalSearch *ls;
	
public:
	VND(DataFile *data, LocalSearch *ls);
	Solution run(Solution s, int max_iter, int *ls_index);

};

#endif //__VND_H__
