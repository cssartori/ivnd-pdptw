#ifndef __ROUTE_H__
#define __ROUTE_H__

#include <vector>

class Route{
private:
	/*Forward vector of nodes' index*/
	std::vector<int> forward;
	/*Backward vector of nodes' index*/
	std::vector<int> backward;
	/*time to reach the given node (it is NOT the time to start the service)*/
	std::vector<double> reach_time; 	
	/*accumulated capacity when reaching node i*/
	std::vector<int> acc_cap; 
	std::vector<double> ftime_slack;
	/*The size of the route, as in how many nodes does it have*/
	int size_r;
	/*Total distance of the route*/
	double distance;
	
public:
	Route(int n);
	/*Return the forward node to the ith in this route*/
	int getForward(int i);
	/*Return the backward node to the ith in this route*/
	int getBackward(int i);
	/*Return the reach time at i*/
	double getReachTime(int i);
	/*Return the accumulated capacity when reaching node i*/
	int getAccCapacity(int i);
	/*Insert node i as the forward of b*/
	void insertNode(int b, int i);
	/*Remove the node i from route*/
	void removeNode(int i);
	/*Update the route distance with the addition cost*/
	void updateDistance(double addCost);	
	/*Update the reach time at node i*/
	void updateReachTime(int i, double addTime);
	void setReachTime(int i, double time);
	/*Update the accumulated capacity to get to node i*/
	void updateAccCapacity(int i, int addCap);
	void setAccCapacity(int i, int cap);
	int getSize();
	double getDistance();
	void setDistance(double d);
	void print();
	
	bool isNodeInRoute(int i);
	
	void setFTimeSlack(int i, double fts);
	double getFTimeSlack(int i);
	
};

#endif //__ROUTE_H__
