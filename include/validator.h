#ifndef __VALIDATOR_H__
#define __VALIDATOR_H__

#include "datafile.h"
#include "solution.h"

class Validator{
public:
	//Constants
	static const int VALID_SOLUTION = 0;
	static const int INVALID_TIME_WINDOW = -1;
	static const int INVALID_DEMAND_SUPPLY = -2;
	static const int INVALID_PAIRING = -3;
	static const int VEHICLE_OVERLOADED = -4;
	static const int INVALID_ROUTE_COST = -5;
	static const int INVALID_SOLUTION_COST = -6;

	//Methods
	/*Validate the solution according to the PDPTW constraints. Return 0 when valid*/
	static int validateSolution(DataFile data, Solution s);
	

};

#endif //__VALIDATOR_H__
