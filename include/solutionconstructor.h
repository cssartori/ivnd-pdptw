#ifndef __SOLUTION_CONSTRUCTOR_H__
#define __SOLUTION_CONSTRUCTOR_H__

#include "datafile.h"
#include "solution.h"
#include "pairmovementtuple.h"

class SolutionConstructor{
private:
	DataFile data;
	
	/*Get the first node to insert in route*/
	int getRouteFirstNode(std::vector<bool> routed);
	/*Insert node u after i in route updating information accordingly*/
	Route insertInRoute(Route route, int b, int i);
	/*Get the PD pair that minimizes distance cost when inserting in route*/
	PairMovementTuple getBestPDPair(Route route, std::vector<bool> routed);
	/*Cost type 1 to insert node u between i and j*/
	double c1(int i, int u, int j);
	/*Check if inserting u after node i in route is a valid movement*/
	bool isValidInsertion(Route route, int i, int u, double rtime_i);

public:
	SolutionConstructor(DataFile data);
	/*Run an adapted version of the insertion heuristic (by Solomon)*/
	Solution insertionHeuristic();	
};

#endif //__SOLUTION_CONSTRUCTOR_H__
