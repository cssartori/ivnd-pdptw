#ifndef __PERTURBATION_H__
#define __PERTURBATION_H__

#include "datafile.h"
#include "solution.h"
#include "pairmovementtuple.h"
#include <random>

typedef struct PairShiftTuple PairShiftTuple;

class Perturbation{
private:
	DataFile data;
	int rseed; //random seed
	std::default_random_engine generator; /*Random number generator engine*/
		
	
	/*Check if inserting u after node i in route is a valid movement*/
	bool isValidInsertion(Route *route, int i, int u, double rtime_i);
	Route insertInRoute(Route route, int i, int u);
	/*Remove node u and its delivery pair from route, updating information accordingly*/
	Route removePairFromRoute(Route route, int u);
	PairMovementTuple searchInsertionLocation(Route route, int u);
	/*Cost to insert node u between i and j*/
	double c_in(int i, int u, int j);
	/*Cost to remove node u between i and j*/
	double c_rm(int i, int u, int j);
	PairMovementTuple searchInsertionLocation2(Route *route, int u);
	
	
public:
	Perturbation(DataFile data, int rseed=1);
	Solution doubleSwap(Solution s, int n_swaps=2);
};

#endif //__PERTURBATION_H__
