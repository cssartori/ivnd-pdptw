#ifndef __NODE_H__
#define __NODE_H__

#include <utility>

const int PICKUP_NODE = 1;
const int DELIVERY_NODE = 2;
const int DEPOT_NODE = 3;

class Node{
private:
	/*Id of the node (0 is the depot)*/
	int id;
	/*Position (x,y) of the node*/
	std::pair<int, int> position;
	/*Demand of the given node (negative values are for delivery points)*/
	int demand;
	/*Time window of this node (earliest, latest)*/
	std::pair<int, int> time_window;
	/*Service time (i.e., how long the vehicle stays at this node)*/
	int service_time;
	/*The pair of this node, if it is a pickup point, the pair is a delivery and vice-versa. By default, the pair of the depot is the depot.*/
	int pdpair;
	/*Indicates the type of this node, being a pickup, delivery or depot*/
	int type;
	
public:
	Node(int id, int x, int y, int demand, int earliest_tw, int latest_tw, int service_time, int pdpair);
	/*Returns the node id*/
	int getId();
	/*Returns the position x or y of this node*/
	int getPosX();
	int getPosY();
	/*Returns the demand of this node (negative values are for delivery points)*/
	int getDemand();
	/*Returns the earliest or latest time window value for this node*/
	int getETW();
	int getLTW();
	/*Returns the service time of this node*/
	int getST();
	/*Returns the pair of this node*/
	int getPair();
	/*Returns true if the node is a pickup location*/
	int getType();
};

#endif //__NODE_H__
