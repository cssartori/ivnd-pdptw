#ifndef __DATA_FILE_H__
#define __DATA_FILE_H__

#include <vector>
#include <string>
#include "node.h"
#include "solution.h"

class DataFile{
private:
	/*Number of pickup requests in the problem*/
	int n_pickups;
	/*Capacity of each vehicle (homogenous fleet) in the problem*/
	int vehicles_capacity;
	/*Number of vehicles avaliable*/
	int num_vehicles;
	/*Vector containing all nodes (0 is the depot)*/
	std::vector<Node> nodes;
	/*Matrix of times from node i to j*/
	std::vector< std::vector<double> > time;
	/*Matrix of distances from node i to j*/
	std::vector< std::vector<double> > distance;
	/*Name of the file containing the data*/
	std::string filename;
		
public:
	DataFile(std::string filename);
	/*Read the file given in the filename*/
	int readData();
	/*Read real world Instances data (with double precision position of nodes) given in the filename*/
	int readRealWorldData();
	/*Return the number of pickup requests in this problem*/
	int getNumPickups();
	/*Returns the capacity of the vehicles in the problem (homogeneous fleet)*/
	int getVehiclesCapacity();
	/*Returns the total number of vehicles in the problem*/
	int getNumVehicles();
	/*Returns the vector of nodes in the problem*/
	std::vector<Node> getNodes();
	/*Return the number of nodes*/
	int getNumNodes();
	/*Returns the ith node in the problem*/
	Node& getNode(int i);
	/*Returns the time to go from node i to j*/
	double getTime(int i, int j);
	/*Returns the distance to go from node i to j*/
	double getDistance(int i, int j);
	/*Returns the file name*/
	std::string getFilename(); 
	/*Write solution to file*/
	int writeSolutionFile(std::string sfilename, Solution s);
	/*Read solution file and return a solution*/
	int readSolutionFile(std::string sfilename, Solution *s);
	/*Write the data read to the GLPK data format, to be used by a LP formulation*/
	int writeAsGLPKData(std::string gfilename);
	/*Prints all the data read*/
	void print();
	void generateOneRouteInstance(Solution s);
	int tspdptw(std::string tfilename);

};

#endif //__DATA_FILE_H__

