#ifndef __CONSTANTS_H
#define __CONSTANTS_H

const int NO_NODE = -1;
const int NO_ROUTE = -1;

const int INF = 1e4;

static const char AUTHOR_NAME[] = "Carlo S. Sartori";

static const int SHIFT_LS_INDX = 0;
static const int SHIFTR2_LS_INDX = 1;
static const int REARRANGE_LS_INDX = 2;
static const int EXCHANGE_LS_INDX = 3;

#endif //__CONSTANTS_H


