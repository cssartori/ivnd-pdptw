#ifndef __LOCAL_SEARCH_H__
#define __LOCAL_SEARCH_H__

#include "datafile.h"
#include "solution.h"
#include "pairmovementtuple.h"
#include "movementcache.h"

class LocalSearch{
private:
	DataFile data;
	MovementCache shift_cache;		
	MovementCache shiftR2_cache;
	MovementCache exchange_cache;
	MovementCache rearrange_cache;
	

	/*Check if inserting u after node i in route is a valid movement*/
	bool isValidInsertion(Route *route, int i, int u, double rtime_i);
	void insertInRoute(Route *route, int i, int u);
	/*Remove node u and its delivery pair from route, updating information accordingly*/
	Route removePairFromRoute(Route route, int u);
	/*Cost to insert node u between i and j*/
	double c_in(int i, int u, int j);
	/*Cost to remove node u between i and j*/
	double c_rm(int i, int u, int j);
	void setModifiedRoutesCaches(int r1, int r2);
	void clearRoutesCache(int r1, int r2);
	
	void calculateFTimeSlack(Route *route);
	PairMovementTuple searchInsertionLocation2(Route *route, int u);
	
public:
	LocalSearch(DataFile data);
	/*Shift pairs between routes, choosing the shift that minimizes the total cost*/
	Solution pdShift(Solution s);
	Solution pdShiftR2(Solution s);
	Solution pdExchange(Solution s);
	Solution pdRearrange(Solution s);

	void initMovementCaches(Solution *s);
	void clearMovementCaches();
	Solution calculateAllFTimeSlack(Solution s);

};

#endif //__LOCAL_SEARCH_H__
