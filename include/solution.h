#ifndef __SOLUTION_H__
#define __SOLUTION_H__

#include <vector>
#include "route.h"

class Solution{
private:
	/*Routes in this solution*/
	std::vector<Route> routes;
	/*Total distance of this solution*/
	double distance;
	
public:
	Solution();
	/*Return the rth route in this solution*/
	Route getRoute(int r);
	/*Return the distance of this solution*/
	double getDistance();
	/*Return the number of routes in this solution*/  
	int getNumRoutes();
	/*Add a route to the solution*/
	void addRoute(Route new_route);
	/*Updates the rth route replacing it by new_route. Updates total solution distance accordingly.*/
	void updateRoute(int r, Route new_route);
	void removeRoute(int r);
	/*Overload of comparison functions to compare two solutions*/
	bool operator<(Solution& s);
	bool operator<=(Solution& s);
	bool operator>(Solution& s);
	bool operator>=(Solution& s);
	bool operator==(Solution& s);
	void setRoute(int r, Route route);
};

#endif //__SOLUTION_H__
