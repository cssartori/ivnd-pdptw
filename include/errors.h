#ifndef __ERRORS_H__
#define __ERRORS_H__

#include <string>

static std::string ROUTE_INDX_OFR_MSG = "Index out of range for route in solution.";
static std::string NODE_INDX_OFR_MSG = "Index out of range for node in route.";
static std::string NODE_INDX_NOT_IN_ROUTE_MSG = "Node not in required route.";
static std::string NODE_ALREADY_IN_ROUTE_MSG = "Node is already in this route, can't insert it again.";
static std::string ROUTE_NO_NODE_MSG = "Route with no nodes.";
 
static std::string NO_FEASIBLE_SOL_MSG = "No feasible solution could be found.";
#endif //__ERRORS_H__
