#-------------Input Information---------------
#Insert documentation
#Two depots are needed because of the time window constraints,
#since h[i,k] indicates the time at which the vehicle k arrives at node i
#if there was only one depot (say, node 0) than it would have h[0,k] = 0 
#at the beginning and it would be necessary a h[0,k] > 0 at the end, what is impossible

#---------------------------------------------

#Number n of requests
param n;

#Maximum number of vehicles available
param Kmax;

#Capacity of each vehicle
param Q;

#Set of vehicles
set K := {1 .. Kmax};

#Set of n pickups
set P;
#Set of n deliveries
set R;

#Delivery location of each pickup
param dev{p in P};

#Set of 2 depots
set E := {0, 2*n+1};

#Set of all 2n+2 nodes
set V := P union R union E; 

#Time matrix
param T{i in V, j in V};

#Distance matrix
param D{i in V, j in V};

#cost of travelling 1 unit of distance
param c;

#cost of using a vehicle
param delta;

#Demand of each node
param O{i in V};

#Service time required by each node
param S{i in V};

#Earliest time windows
param A{i in V};

#Latest time windows
param B{i in V};

#A very big constant
param W;
#------------------Variables--------------------

#decision variable x: 1, if arc (i,j) is traversed by vehicle k; 0, otherwise
var x{i in V, j in V, k in K} binary;

#decision variable y: 1, if vehicle k is used; 0, otherwise
var y{k in K} binary;

#Variable indicating the time at which vehicle k arrives at node i
var h{i in V, k in K};

#Variable indicating the capacity used of the vehicle k when reaching node i
var q{i in V, k in K} integer;

#-----------------Formulation-------------------

#OF: minimize the costs related to distance travelled and vehicle usage
minimize costs: sum{i in V}(sum{j in V}(sum{k in K} x[i,j,k]*D[i,j]*c)) + delta*sum{k in K}(y[k]);

s.t.
#Restriction 1: All requests must be attended
r1{i in P}: sum{k in K}( sum{j in V: j!=i}( x[i,j,k] )) = 1; 

#Restriction 2: If a vehicle serves node i, it must also serve node dev(i)
r2{i in P, k in K}: sum{j in V: j!=i}( x[i,j,k] ) - sum{j in V: j!=i}( x[dev[i],j,k] ) = 0;

#Restriction 3: If a vehicles leaves the depot, assign y[k]
r3{k in K}: sum{j in V: j!=0}( x[0,j,k] ) = y[k];

#Restriction 4: If a vehicles arrives at the final depot, assign y[k]
r4{k in K}: sum{j in V: j!=2*n+1}( x[j,2*n+1,k] ) = y[k];

#Restrictions 5 and 6: Link the y variable with the x variable
r5{k in K}: sum{i in V}( sum{j in V: j!=i}( x[i,j,k] )) <= W*y[k];
r6{k in K}: sum{i in V}( sum{j in V: j!=i}( x[i,j,k] )) >= y[k];

#Restriction 7: If a vehicles arrives at node i, it must leave node i (except for 0 and 2n+1)
r7{k in K, i in P union R}: sum{j in V: j!=i}( x[i,j,k] ) -  sum{j in V: j!=i}( x[j,i,k] ) = 0;

#Restriction 8: Precedence constraint on time windows
r8{i in V, j in V, k in K: j!=i}: h[j,k] >= h[i,k] + (T[i,j]+S[i])*x[i,j,k] - W*(1-x[i,j,k]);

#Restriction 9: Guarantees that the time of serving node i is smaller than te time to serve dev[i]
r9{i in P, k in K}: h[i,k]+T[i,dev[i]]*sum{j in V: j!=i}( x[i,j,k] ) <= h[dev[i], k];

#Restriction 10 and 11: Limitation by time windows of the variables h
r10{i in V, k in K}: h[i,k] >= A[i];
r11{i in V, k in K}: h[i,k] <= B[i];

#Restriction 12: Updates the capacity of the vehicle in each node
r12{i in V, j in V, k in K: j!=i}: q[j,k] >= q[i,k] + O[j] - W*(1-x[i,j,k]);

#Restrictions 13 and 14: Check if the vehicle does not exceed its capacity and that it does not get negative capacity
r13{i in V, k in K}: max(0,O[i])*sum{j in V: j!=i}( x[i,j,k] ) <= q[i,k];
r14{i in V, k in K}: min(Q,Q+O[i])*sum{j in V: j!=i}( x[i,j,k] ) >= q[i,k];

end;








