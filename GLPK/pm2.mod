#Two-index formulation of the PDPTW


#---------------------------------------------

#Number n of requests
param n;

#Maximum number of vehicles available
param Kmax;

#Capacity of each vehicle
param Q;

#Set of vehicles
set K := {1 .. Kmax};

#Set of n pickups
set P;
#Set of n deliveries
set R;

#Delivery location of each pickup
param dev{p in P};

#Set of 2 depots
set E := {0, 2*n+1};

#Set of all 2n+2 nodes
set V := P union R union E; 

#Time matrix
param T{i in V, j in V};

#Distance matrix
param D{i in V, j in V};

#cost of travelling 1 unit of distance
param c;

#cost of using a vehicle
param delta;

#Demand of each node
param O{i in V};

#Service time required by each node
param S{i in V};

#Earliest time windows
param A{i in V};

#Latest time windows
param B{i in V};

#A very big constant
param W;
#------------------Variables--------------------

#decision variable x: 1, if arc (i,j) is traversed by vehicle k; 0, otherwise
var x{i in V, j in V} binary;

#decision variable y: 1, if vehicle k is used; 0, otherwise
var y integer;

#Variable indicating the time at which vehicle k arrives at node i
var h{i in V};

#Variable indicating the capacity used of the vehicle k when reaching node i
var q{i in V} integer;

var v{i in P union R};

#-----------------Formulation-------------------

#OF: minimize the costs related to distance travelled and vehicle usage
minimize costs: sum{j in P} delta*x[0,j] + sum{i in V} sum{j in V} x[i,j]*D[i,j];

s.t.
#Restriction 1: All requests must be attended
r1{i in P union R}: sum{j in V: j!=i}( x[i,j] ) = 1; 
r1l{j in P union R}: sum{i in V: j!=i}( x[i,j] ) = 1; 

r7{i in P union R}: sum{j in V: j!=i}( x[i,j] ) -  sum{j in V: j!=i}( x[j,i] ) = 0;

#Restriction 8: Precedence constraint on time windows
r8{i in V, j in V: j!=i}: h[j] >= h[i] + (T[i,j]+S[i]) - W*(1-x[i,j]);

#Restriction 9: Guarantees that the time of serving node i is smaller than te time to serve dev[i]
r9{i in P}: h[i]+S[i]+T[i,dev[i]] <= h[dev[i]];

#Restriction 10 and 11: Limitation by time windows of the variables h
r10{i in V}: h[i] >= A[i];
r11{i in V}: h[i] <= B[i];

#Restriction 12: Updates the capacity of the vehicle in each node
r12{i in V, j in V: j!=i}: q[j] >= q[i] + O[j] - W*(1-x[i,j]);

#Restrictions 13 and 14: Check if the vehicle does not exceed its capacity and that it does not get negative capacity
r13{i in V}: max(0,O[i]) <= q[i];
r14{i in V}: min(Q,Q+O[i]) >= q[i];


r15{i in P}: v[i] = v[dev[i]];
r16{j in P union R}: v[j] >= j*x[0,j];
r17{j in P union R}: v[j] <= j*x[0,j] - (2*n+1)*(x[0,j] - 1);
r18{i in P union R, j in P union R}: v[j] >= v[i] + (2*n+1)*(x[i,j] - 1);
r19{i in P union R, j in P union R}: v[j] <= v[i] + (2*n+1)*(1 - x[i,j]);

r21: sum{j in R} x[j, 2*n+1] = sum{j in P} x[0,j];

r22: sum{j in P} x[0,j] >= 1;
r23: sum{j in P} x[0,j] <= Kmax;
end;








