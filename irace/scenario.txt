###################################################### -*- mode: r -*- #####
## Scenario setup for Iterated Race (iRace).
############################################################################

## File that contains the description of the parameters.
parameterFile = "./parameters.txt"

## Directory where the programs will be run.
execDir = "../"

## The script called for each configuration that launches the program to be
## tuned.  See templates/target-runner.tmpl
targetRunner = "../main"

## File with a list of instances and (optionally) parameters.
## If empty or NULL, do not use a file.
trainInstancesFile = "./training.txt"

## The maximum number of runs (invocations of targetRunner) that will
## performed. It determines the (maximum) budget of experiments for the tuning.
maxTime = 320000

## Indicates the number of decimal places to be considered for the
## real parameters.
digits = 3

debugLevel = 2

parallel = 4


