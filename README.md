# IVND applied to the PDPTW #

This repository refers to the paper *An Iterated Variable Neighborhood Descent Algorithm applied to the Pickup and Delivery Problem with Time Windows*, which is based in the bachelor thesis in computer science of Carlo Sartori, developed in 2016 at *Universidade Federal do Rio Grande do Sul* (UFRGS).

## Implementation ##

The implementation has been done in C++. The code is available under the folder src/ and include/ (the latter for headers). It can be compiled under UNIX systems by a Makefile available with the code.

Compilation and tests have been done under an Ubuntu 16.04 LTS operating system.

### Compiling ###

When compiling, the Makefile creates three executable files.

1. **main**: receives up to one instance file, all parameters and runs the IVND algorithm. It can print the solution to a file in the SINTEF format, as well as generate a GLPK data file for the mathematical formulation;

2. **runner**: receives a directory as parameter and the number of runs *n*. Executes the IVND for all instance files in the directory, repeating *n* times for each one. It saves the output to a given file. Used mainly for tests;

3. **tabler**: used to generate the Latex tables used in the work and available under the folder results/.

## References ##

The original bachelor thesis in computer science is available at [Lume UFRGS](http://www.lume.ufrgs.br/handle/10183/150897).

Information about the standard set of instances is available over [SINTEF website](https://www.sintef.no/projectweb/top/pdptw/li-lim-benchmark/documentation/).